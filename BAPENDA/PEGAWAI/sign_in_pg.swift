//
//  sign_in_pg.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 05/12/22.
//

import SwiftUI

struct sign_in_pg: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var cap: String = ""
    @State private var cap2: String = ""
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
                        NavigationLink(destination: welcome_pg().navigationBarHidden(true)) {
                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding([.bottom],5)
                                        }
                        
                        Text("Masuk")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                            
                        
                        Text("Selamat datang di Aplikasi Pajak Daerah Kota Malang")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            Text("Username/Email")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .multilineTextAlignment(.leading)
                                .padding([.leading])
                                .padding([.top],20)
                            
                            TextField("Username/Email", text: $username)
                                .padding(15)
                                .foregroundColor(Color.gray)
                                .cornerRadius(15)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color("fontblack"), lineWidth: 1)
                                )
                                .padding([.leading, .trailing],10)
                                .padding([.bottom],10)
                            
                            Text("Kata Sandi")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .multilineTextAlignment(.leading)
                                .padding([.leading])
                            
                            TextField("Kata Sandi", text: $password)
                                .padding(15)
                                .foregroundColor(Color.gray)
                                .cornerRadius(15)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color("fontblack"), lineWidth: 1)
                                )
                                .padding([.leading, .trailing],10)
                            
                            Text("Lupa Password?")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .frame(maxWidth: .infinity, alignment: .trailing)
                                .multilineTextAlignment(.trailing)
                                .padding([.trailing])
                            
                            HStack() {
                                
                                TextField("Masukkan Angka", text: $cap)
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                                
                                TextField("", text: $cap2)
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                            Spacer()
                            
                            NavigationLink(
                                destination: main_view_pg().navigationBarHidden(true),
                                label: {
                                    Text("Masuk")
                                        .font(.title3)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("PrimaryColor"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color("PrimaryColor"), lineWidth: 1)
                                        )
                                    
                                })
                            .navigationBarHidden(true)
                            .padding([.leading, .trailing, .bottom],10)
                            .padding([.bottom],10)
                          
                        }
                    }
                }
            }
        }
    }
}

struct sign_in_pg_Previews: PreviewProvider {
    static var previews: some View {
        sign_in_pg()
    }
}
