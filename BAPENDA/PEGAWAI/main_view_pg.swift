//
//  main_view_pg.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 05/12/22.
//

import SwiftUI

struct main_view_pg: View {
        @State var selectedTab = 0
        var body: some View {
            TabView(selection: $selectedTab) {
                
                home_pg()
                    .tag(0)
                    .tabItem {
                        Image(systemName: "house.fill")
//                            Text("Beranda")
                    }
             
                wp()
                    .tag(1)
                    .tabItem {
                        Image(systemName: "map")
//                        Text("Wajib Pajak")
                    }
                
                map()
                    .tag(2)
                    .tabItem {
                        Image(systemName: "location")
//                        Text("Notifikasi")
                    }
                
                profile_pg()
                    .tag(3)
                    .tabItem {
                        Image(systemName: "person.fill")
//                        Text("Profil")
                    }
            }.onAppear() {
    //            UITabBar.appearance().backgroundColor = UIColor.gray
                let standardAppearance = UITabBarAppearance()
                standardAppearance.backgroundColor = UIColor(Color.white)
                standardAppearance.shadowColor = UIColor(Color.gray)
                
                let itemAppearance = UITabBarItemAppearance()
                itemAppearance.normal.iconColor = UIColor(Color.black)
                itemAppearance.selected.iconColor = UIColor(Color.blue)
                itemAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(Color.black)]
                itemAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(Color.blue)]
                standardAppearance.inlineLayoutAppearance = itemAppearance
                standardAppearance.stackedLayoutAppearance = itemAppearance
                standardAppearance.compactInlineLayoutAppearance = itemAppearance
                UITabBar.appearance().standardAppearance = standardAppearance
            }
        }
}

struct main_view_pg_Previews: PreviewProvider {
    static var previews: some View {
        main_view_pg()
    }
}
