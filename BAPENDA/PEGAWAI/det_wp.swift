//
//  det_wp.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 07/12/22.
//  Copyright © 2022 MC Teknologi. All rights reserved.
//

import SwiftUI

struct det_wp: View {
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        HStack(){
                            NavigationLink(destination: wp().navigationBarHidden(true)) {
                                Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                    .frame(alignment: .leading)
                                    .padding([.bottom],5)
                            }
                            
                            Text("Daftar Wajib Pajak")
                                .font(.system(size: 20))
                                .foregroundColor(Color("fontwhite"))
                                .fontWeight(.bold)
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .multilineTextAlignment(.leading)
                                .padding([.leading])
                                .padding([.bottom],5)
                        }
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            ScrollView(.vertical){
                                    Text("Hasil Pencarian: Keyword")
                                        .font(.system(size: 15))
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("fontblack"))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .padding()
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack {
                                                Text("NPWPD: 1234.50.120.")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("NOP : 35.73.050.001.001.0001.0")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("Nama Wajib Pajak: Teuku")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("Lokasi: Jl. Mayjend Sungkono No.3")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                            }
                                            Image(uiImage: #imageLiteral(resourceName: "icohotel"))
                                                .frame(maxWidth: 100, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 100)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack {
                                                Text("NPWPD: 1234.50.120.")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("NOP : 35.73.050.001.001.0001.0")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("Nama Wajib Pajak: Teuku")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("Lokasi: Jl. Mayjend Sungkono No.3")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                            }
                                            Image(uiImage: #imageLiteral(resourceName: "icohotel"))
                                                .frame(maxWidth: 100, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 100)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack {
                                                Text("NPWPD: 1234.50.120.")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("NOP : 35.73.050.001.001.0001.0")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("Nama Wajib Pajak: Teuku")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                Text("Lokasi: Jl. Mayjend Sungkono No.3")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                            }
                                            VStack{
                                                Image(uiImage: #imageLiteral(resourceName: "icohotel"))
                                                    .frame(maxWidth: 100, alignment: .center)
                                                NavigationLink(
                                                    destination: {}) {
                                                        Text("Tutup")
                                                            .font(.system(size: 8))
                                                            .foregroundColor(Color.red)
                                                            
                                                    }
                                                    .background(Color.red.opacity(0.3))
                                                    .cornerRadius(10)
                                                    .padding()
                                            }
                                            
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 100)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                
                                Spacer()
                            }
                        }
                    }
                }
            }
        }
    }
}

struct det_wp_Previews: PreviewProvider {
    static var previews: some View {
        det_wp()
    }
}
