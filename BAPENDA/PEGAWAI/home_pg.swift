//
//  home_pg.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 05/12/22.
//

import SwiftUI

struct home_pg: View {
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
//                        NavigationLink(destination: welcome_page().navigationBarHidden(true)) {
//                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
//                                .frame(maxWidth: .infinity, alignment: .leading)
//                                .padding([.bottom],5)
//                        }
                        
                        Text("Aplikasi Pajak")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                        
                        
                        Text("Aplikasi Pajak Daerah Kota Malang")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                        
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            ScrollView(.vertical){
                                ZStack{
                                    Rectangle()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                        .edgesIgnoringSafeArea(.all)
                                    VStack{
                                        HStack{
                                            VStack{
                                                Image(uiImage: #imageLiteral(resourceName: "icopeople"))
                                                    .frame(maxWidth: .infinity, alignment: .center)
                                                
                                                Text("Jumlah Wajib Pajak")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .center)
                                                    .multilineTextAlignment(.center)
                                                
                                            }
                                            .padding(.leading, 10)
                                            .padding(.trailing, 50)
                                            .padding(.top, 10)
                                            
                                            Text("xx.xxx.xxx")
                                                .font(.system(size: 15))
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("fontblack"))
                                                .frame(alignment: .trailing)
                                                .multilineTextAlignment(.trailing)
                                                .padding(.trailing, 10)
                                        }
                                        Divider()
                                            .padding([.leading, .trailing])
                                            .padding([.bottom, .top],5)
                                        dash_home()
                                    }
                                }.padding(10)
                                
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak Hotel")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icohotel"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak Resto")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icoresto"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak Parkir")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icoparkir"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak Reklame")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icoreklame"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak Air Tanah")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icoairtanah"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak Hiburan")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icohiburan"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak Penerangan Jalan")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icopenerangan"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                NavigationLink(
                                    destination: {}) {
                                        HStack{
                                            VStack() {
                                                Text("Pajak PBB")
                                                    .font(.system(size: 15))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                                Image(uiImage: #imageLiteral(resourceName: "icopbb"))
                                            }.frame(maxWidth: .infinity, alignment: .center)
                                            VStack() {
                                                Text("Realisasi")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Target")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                Text("Rp. xxx.xxx.xxx.xxx")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.trailing)
                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                            Image(uiImage: #imageLiteral(resourceName: "icoprogress"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                        }.padding([.leading, .trailing], 10)
                                        
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding(10)
                                
                                Spacer()
                            }
                        }
                    }
                }
            }
        }
    }
}

struct home_pg_Previews: PreviewProvider {
    static var previews: some View {
        home_pg()
    }
}
struct dash_home: View {
    var body: some View {
       HStack{
           Group{
               NavigationLink(
                   destination: det_home_hotel().navigationBarHidden(true)) {
                       VStack{
                           Text("Pajak Hotel")
                               .font(.system(size: 10))
                               .fontWeight(.bold)
                               .foregroundColor(Color("fontblack"))
                               .frame(maxWidth: .infinity, alignment: .leading)
                               .padding([.trailing, .leading, .top], 10)
                           HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                               Image(uiImage: #imageLiteral(resourceName: "icohotel"))
                           }.padding(.top, -15)
                       }
                   }
                   .frame(maxWidth: .infinity)
                   .background(Color.gray.opacity(0.3))
                   .cornerRadius(10)
                   .shadow(
                       color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                   )
                   .edgesIgnoringSafeArea(.all)
           }
           Group{
               NavigationLink(
                   destination: det_home_resto().navigationBarHidden(true)) {
                       VStack{
                           Text("Pajak Resto")
                               .font(.system(size: 10))
                               .fontWeight(.bold)
                               .foregroundColor(Color("fontblack"))
                               .frame(maxWidth: .infinity, alignment: .leading)
                               .padding([.trailing, .leading, .top], 10)
                           HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                               Image(uiImage: #imageLiteral(resourceName: "icoresto"))
                           }.padding(.top, -15)
                       }
                   }
                   .frame(maxWidth: .infinity)
                   .background(Color.gray.opacity(0.3))
                   .cornerRadius(10)
                   .shadow(
                       color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                   )
                   .edgesIgnoringSafeArea(.all)
           }
           Group{
               NavigationLink(
                   destination: det_home_parkir().navigationBarHidden(true)) {
                       VStack{
                           Text("Pajak Parkir")
                               .font(.system(size: 10))
                               .fontWeight(.bold)
                               .foregroundColor(Color("fontblack"))
                               .frame(maxWidth: .infinity, alignment: .leading)
                               .padding([.trailing, .leading, .top], 10)
                           HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                               Image(uiImage: #imageLiteral(resourceName: "icoparkir"))
                           }.padding(.top, -15)
                       }
                   }
                   .frame(maxWidth: .infinity)
                   .background(Color.gray.opacity(0.3))
                   .cornerRadius(10)
                   .shadow(
                       color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                   )
                   .edgesIgnoringSafeArea(.all)
           }
       }
       .padding([.leading, .trailing], 10)
       .padding([.top, .bottom], 5)
       
       HStack{
           NavigationLink(
               destination: det_home_hiburan().navigationBarHidden(true)) {
                   VStack{
                       Text("Pajak Hiburan")
                           .font(.system(size: 10))
                           .fontWeight(.bold)
                           .foregroundColor(Color("fontblack"))
                           .frame(maxWidth: .infinity, alignment: .leading)
                           .padding([.trailing, .leading, .top], 10)
                       HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                           Image(uiImage: #imageLiteral(resourceName: "icohiburan"))
                       }.padding(.top, -15)
                   }
           }
               .frame(maxWidth: .infinity)
               .background(Color.gray.opacity(0.3))
               .cornerRadius(10)
               .shadow(
                   color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
               )
               .edgesIgnoringSafeArea(.all)
           NavigationLink(
               destination: det_home_airtanah().navigationBarHidden(true)) {
                   VStack{
                       Text("Pajak Air Tanah")
                           .font(.system(size: 10))
                           .fontWeight(.bold)
                           .foregroundColor(Color("fontblack"))
                           .frame(maxWidth: .infinity, alignment: .leading)
                           .padding([.trailing, .leading, .top], 10)
                       HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                           Image(uiImage: #imageLiteral(resourceName: "icoairtanah"))
                       }.padding(.top, -15)
                   }
           }
               .frame(maxWidth: .infinity)
               .background(Color.gray.opacity(0.3))
               .cornerRadius(10)
               .shadow(
                   color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
               )
               .edgesIgnoringSafeArea(.all)
           NavigationLink(
               destination: det_home_peneranganjalan().navigationBarHidden(true)) {
                   VStack{
                       Text("Pajak Penerangan Jalan")
                           .font(.system(size: 10))
                           .fontWeight(.bold)
                           .foregroundColor(Color("fontblack"))
                           .frame(maxWidth: .infinity, alignment: .leading)
                           .padding([.trailing, .leading, .top], 10)
                       HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                           Image(uiImage: #imageLiteral(resourceName: "icopenerangan"))
                       }.padding(.top, -15)
                   }
           }
               .frame(maxWidth: .infinity)
               .background(Color.gray.opacity(0.3))
               .cornerRadius(10)
               .shadow(
                   color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
               )
               .edgesIgnoringSafeArea(.all)
       }
       .padding([.leading, .trailing], 10)
       .padding([.top, .bottom], 5)
       HStack{
           NavigationLink(
               destination: det_home_reklame().navigationBarHidden(true)) {
                   VStack{
                       Text("Pajak Reklame")
                           .font(.system(size: 10))
                           .fontWeight(.bold)
                           .foregroundColor(Color("fontblack"))
                           .frame(maxWidth: .infinity, alignment: .leading)
                           .padding([.trailing, .leading, .top], 10)
                       HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                           Image(uiImage: #imageLiteral(resourceName: "icoreklame"))
                       }.padding(.top, -15)
                   }
           }
               .frame(maxWidth: .infinity)
               .background(Color.gray.opacity(0.3))
               .cornerRadius(10)
               .shadow(
                   color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
               )
               .edgesIgnoringSafeArea(.all)
           NavigationLink(
               destination: det_home_pbb().navigationBarHidden(true)) {
                   VStack{
                       Text("Pajak PBB")
                           .font(.system(size: 10))
                           .fontWeight(.bold)
                           .foregroundColor(Color("fontblack"))
                           .frame(maxWidth: .infinity, alignment: .leading)
                           .padding([.trailing, .leading, .top], 10)
                       HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                           Image(uiImage: #imageLiteral(resourceName: "icopbb"))
                       }.padding(.top, -15)
                   }
           }
               .frame(maxWidth: .infinity)
               .background(Color.gray.opacity(0.3))
               .cornerRadius(10)
               .shadow(
                   color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
               )
               .edgesIgnoringSafeArea(.all)
           NavigationLink(
               destination: det_home_bphtb().navigationBarHidden(true)) {
                   VStack{
                       Text("Pajak BPHTB")
                           .font(.system(size: 10))
                           .fontWeight(.bold)
                           .foregroundColor(Color("fontblack"))
                           .frame(maxWidth: .infinity, alignment: .leading)
                           .padding([.trailing, .leading, .top], 10)
                       HStack{
                               Text("xx.xxx")
                                   .font(.system(size: 10))
                                   .foregroundColor(Color("fontblack"))
                                   .frame(maxWidth: .infinity, alignment: .leading)
                                   .padding(.leading, 10)
                           Image(uiImage: #imageLiteral(resourceName: "icobphtb"))
                       }.padding(.top, -15)
                   }
           }
               .frame(maxWidth: .infinity)
               .background(Color.gray.opacity(0.3))
               .cornerRadius(10)
               .shadow(
                   color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
               )
               .edgesIgnoringSafeArea(.all)
       }
       .padding([.leading, .trailing], 10)
       .padding([.top, .bottom], 5)
    }
}
