//
//  wp.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 05/12/22.
//

import SwiftUI

struct wp: View {
    @State private var textfield: String = ""
    @State private var kelurahan = false
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
//                        NavigationLink(destination: welcome_page().navigationBarHidden(true)) {
//                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
//                                .frame(maxWidth: .infinity, alignment: .leading)
//                                .padding([.bottom],5)
//                        }
                        
                        Text("Aplikasi Pajak")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                        
                        
                        Text("Aplikasi Pajak Daerah Kota Malang")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                        
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            ScrollView(.vertical){
                                ZStack{
                                    Rectangle()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("fontgray").opacity(0.3))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                        .edgesIgnoringSafeArea(.all)
                                    VStack{
                                        Text("Profil Wajib Pajak dan Peta Lokasi")
                                            .font(.system(size: 17))
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("fontblack"))
                                            .frame(maxWidth: .infinity, alignment: .center)
                                            .multilineTextAlignment(.center)
                                        
                                        Text("Wajib Pajak")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],20)
                                        
                                        HStack{
                                            Group{
                                                Image(systemName:"magnifyingglass")
                                                    .frame(alignment: .trailing)
                                                    .frame(width: 40, height: 40, alignment: .center)
                                                    .foregroundColor(Color.black)
                                                    .padding(.leading, 10)
                                                TextField("Username/Email", text: $textfield)
                                                    .padding(15)
                                                    .foregroundColor(Color.gray)
                                            }
                                        }.cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                       
                                        
                                        Text("Jenis Pajak")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                        
                                        DisclosureGroup("Jenis Pajak", isExpanded: $kelurahan) {
                                            VStack() {
                                                ForEach(1...5, id: \.self) {
                                                    num in
                                                    Text("\(num)")
                                                        .font(.system(size: 15))
                                                        .padding(.all)
                                                }
                                            }
                                        }.accentColor(Color("fontblack"))
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .padding(.all)
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                        NavigationLink(
                                            destination: det_wp().navigationBarHidden(true),
                                            label: {
                                                Text("Cari Data Wajib Pajak")
                                                    .font(.title3)
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontwhite"))
                                                    .padding()
                                                    .frame(maxWidth: .infinity)
                                                    .background(Color("PrimaryColor"))
                                                    .cornerRadius(10)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 10)
                                                            .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                    )
                                                
                                            })
                                        .navigationBarHidden(true)
                                        .padding([.leading, .trailing, .bottom],10)
                                        .padding([.bottom],10)
                                    }.padding()
                                    
                                }.padding(30)
                                
                                Spacer()
                            }
                        }
                    }
                }
            }
        }
    }
}

struct wp_Previews: PreviewProvider {
    static var previews: some View {
        wp()
    }
}
