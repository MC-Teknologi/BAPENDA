//
//  profile.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI

struct profile: View {
    @State private var tabBar: UITabBar! = nil
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                                Text("Profile")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack(){
                                HStack{
                                    VStack{
                                        Text("Nama Lengkap")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .fontWeight(.bold)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                        Text("Username")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                        Text("Password")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                    }
                                    Image(uiImage: #imageLiteral(resourceName: "edit"))
                                        .resizable()
                                       .frame(width: 30, height: 30)
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                        .padding([.trailing])
                                }
                                Divider()
                                    .padding([.leading, .trailing])
                                    .padding([.bottom, .top],5)
                                HStack{
                                    Image(systemName: "key")
                                        .resizable()
                                       .frame(width: 30, height: 30)
                                        .frame(alignment: .leading)
                                        .foregroundColor(.black)
                                        .padding([.bottom],5)
                                        .padding([.leading])
                                    VStack{
                                        
                                        Text("Ubah Password")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .fontWeight(.bold)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                    }
                                    
                                }
                                Divider()
                                    .padding([.leading, .trailing])
                                    .padding([.bottom, .top],5)
                                NavigationLink(destination: {}){
                                    HStack{
                                        Image(systemName: "rectangle.portrait.and.arrow.right")
                                            .resizable()
                                            .frame(width: 30, height: 30)
                                            .frame(alignment: .leading)
                                            .padding([.bottom],5)
                                            .padding([.leading])
                                            .foregroundColor(Color("fontblack"))
                                        
                                        Text("Keluar Akun")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .fontWeight(.bold)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                    }
                                }
//                                HStack{
//                                    Image(systemName: "rectangle.portrait.and.arrow.right")
//                                        .resizable()
//                                       .frame(width: 30, height: 30)
//                                        .frame(alignment: .leading)
//                                        .padding([.bottom],5)
//                                        .padding([.leading])
//                                        .foregroundColor(Color("fontblack"))
//                                    VStack{
//                                        NavigationLink(destination: register().navigationBarHidden(true)) {
//                                            Text("Keluar Akun")
//                                                .font(.system(size: 15))
//                                                .foregroundColor(Color("fontblack"))
//                                                .fontWeight(.bold)
//                                                .frame(maxWidth: .infinity, alignment: .leading)
//                                                .multilineTextAlignment(.leading)
//                                                .padding([.leading])
//                                        }
//                                    }
//
//                                }
                                Divider()
                                    .padding([.leading, .trailing])
                                    .padding([.bottom, .top],5)
                                Spacer()
                            }.padding(.top, 20)
                        }
                    }
                }
        }
    }
}

struct profile_Previews: PreviewProvider {
    static var previews: some View {
        profile()
    }
}
