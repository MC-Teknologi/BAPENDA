//
//  pdl_det_parkir_pendaftaran_5.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI
import StepperView

struct pdl_det_parkir_pendaftaran_5: View {
    @State private var textfield: String = ""
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State private var showingCredits = false
    @State private var showingCredits2 = false
    @State var isChecked:Bool = false

    
    let steps = [ Text("Pemilik NPWPD").font(.caption).foregroundColor(Color.black),
                  Text("Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Detail Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Lampiran").font(.caption).foregroundColor(Color.black),
                  Text("Selesai").font(.caption).foregroundColor(Color.black)
                ]

    let indicationTypes = [
                            StepperIndicationType.custom(NumberedCircleView(text: "1", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "2", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "3", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "4", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "5", color: Color("PrimaryColor")))
                        ]
    

    
        
    func toggle(){isChecked = !isChecked}
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
//                    Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                        .resizable()
//                        .frame(width: .infinity, height: 200, alignment: .center)
//                        .edgesIgnoringSafeArea(.all)
                    
                    VStack() {
                        ZStack() {
                            
//                            Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                                .resizable()
//                                .frame(width: .infinity, height: 50)
//                                .edgesIgnoringSafeArea(.all)
                            VStack(){
                                HStack(){
                                    NavigationLink(destination: pdl_det_parkir_pendaftaran_4().navigationBarHidden(true)) {
                                        Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                            .frame(alignment: .leading)
                                            .padding([.bottom],5)
                                    }
                                    
                                    Text("Pendaftaran NPWPD")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontwhite"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],5)
                                }
                            }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        }.padding(.bottom, 10)
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Spacer()
                                StepperView()
                                        .addSteps(steps)
                                        .indicators(indicationTypes)
                                        .stepIndicatorMode(StepperMode.horizontal)
                                        .spacing(50)
                                        .lineOptions(StepperLineOptions.custom(1, Color("PrimaryColor")))
                                        .stepLifeCycles([StepLifeCycle.completed, .pending, .pending, .pending, .pending])
                                        .padding(.top, 50)
                                ScrollView(.vertical){
                                    Text("Selesai")
                                        .font(.system(size: 25))
                                        .foregroundColor(Color("fontblack"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],5)
                                        .padding([.top],20)
                                    
                                    
                                    Text("Pendaftaran NPWPD anda sedang diverifikasi Petugas")
                                        .font(.system(size: 18))
                                        .foregroundColor(Color("fontblack"))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                Spacer()
                                    Image(uiImage: #imageLiteral(resourceName: "check"))
                                        .padding([.bottom],20)
                                    
//                                    Text("Pendaftaran Selesai")
//                                        .font(.system(size: 18))
//                                        .foregroundColor(Color("fontblack"))
//                                        .frame(maxWidth: .infinity, alignment: .center)
//                                        .multilineTextAlignment(.center)
                                }
                                Spacer()
                                NavigationLink(
                                    destination: pdl_det_parkir().navigationBarHidden(true),
                                    label: {
                                        Text("Halaman Utama")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("BgColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color("PrimaryColor"))
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                                            )
                                        
                                    })
                                .navigationBarHidden(true)
                                .padding([.leading, .trailing, .bottom],10)
                                .padding([.bottom],10)
                        }
                    }
                }
            }
        }
    }
}

struct pdl_det_parkir_pendaftaran_5_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_parkir_pendaftaran_5()
    }
}
