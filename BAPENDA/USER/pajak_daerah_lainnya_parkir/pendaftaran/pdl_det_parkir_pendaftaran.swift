//
//  pdl_det_parkir_pendaftaran.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 29/11/22.
//

import SwiftUI
import StepperView

struct pdl_det_parkir_pendaftaran: View {
    
    @State private var textfield: String = ""
    @State private var nikn: String = ""
    @State private var nikn2: String = ""
    
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State var isChecked:Bool = false
    
    let steps = [ Text("Pemilik NPWPD").font(.caption).foregroundColor(Color.black),
                  Text("Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Detail Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Lampiran").font(.caption).foregroundColor(Color.black),
                  Text("Selesai").font(.caption).foregroundColor(Color.black)
                ]

    let indicationTypes = [
                            StepperIndicationType.custom(NumberedCircleView(text: "1", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "2", color: Color.white)),
                            StepperIndicationType.custom(NumberedCircleView(text: "3", color: Color.white)),
                            StepperIndicationType.custom(NumberedCircleView(text: "4", color: Color.white)),
                            StepperIndicationType.custom(NumberedCircleView(text: "5", color: Color.white))
                        ]
    

    
        
    func toggle(){isChecked = !isChecked}
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
//                    Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                        .resizable()
//                        .frame(width: .infinity, height: 200, alignment: .center)
//                        .edgesIgnoringSafeArea(.all)
                    
                    VStack() {
                        ZStack() {
                            
//                            Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                                .resizable()
//                                .frame(width: .infinity, height: 50)
//                                .edgesIgnoringSafeArea(.all)
                            VStack(){
                                HStack(){
                                    NavigationLink(destination: pdl_det_parkir().navigationBarHidden(true)) {
                                        Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                            .frame(alignment: .leading)
                                            .padding([.bottom],5)
                                    }
                                    
                                    Text("Pendaftaran NPWPD")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontwhite"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],5)
                                }
                            }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        }.padding(.bottom, 10)
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Spacer()
                                StepperView()
                                        .addSteps(steps)
                                        .indicators(indicationTypes)
                                        .stepIndicatorMode(StepperMode.horizontal)
                                        .spacing(50)
                                        .lineOptions(StepperLineOptions.custom(1, Color("PrimaryColor")))
                                        .stepLifeCycles([StepLifeCycle.completed, .pending, .pending, .pending, .pending])
                                        .padding(.top, 50)
                                
                                    Text("Data Pemilik Objek Pajak")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("fontblack"))
                                        
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom, .top],10)
                                    Divider()
                                        .padding([.leading, .trailing])
                                        .padding([.bottom, .top],5)
                                    Picker("", selection: $segmentationSelection) {
                                        Text("Perorangan").tag(0)
                                        Text("Perusahaan").tag(1)
                                            }
                                    .pickerStyle(SegmentedPickerStyle())
                                    .onAppear{
                                        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(named: "NavColor")
                                    }
                                    .padding([.leading, .trailing])
                                    Button(action: toggle){
                                        HStack{
                                            Image(systemName: isChecked ? "checkmark.square": "square")
                                            Text("Data Pemilik Sama dengan Data Pengguna")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                        }.padding(.top, 10)
                                    }
                                    TabView(selection: $segmentationSelection){
                                        pdl_det_parkir_pnd_perorangan().tag(0)
                                        pdl_det_parkir_pdn_perusahaan().tag(1)
                                    }
                                    .tabViewStyle(.page(indexDisplayMode: .never))
//                                    .frame(width: .infinity)
                                    .animation(.easeIn, value: segmentationSelection)
                                        
                                    
                                    Spacer()
                            
                        }
                    }
                }
            }
        }
    }
}

struct pdl_det_parkir_pendaftaran_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_parkir_pendaftaran()
    }
}
