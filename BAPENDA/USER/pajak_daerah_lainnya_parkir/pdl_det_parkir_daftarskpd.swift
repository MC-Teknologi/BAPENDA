//
//  pdl_det_hotel_daftarskpd.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 28/11/22.
//

import SwiftUI

struct pdl_det_parkir_daftarskpd: View {
    @State private var isExpanded = false
    let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .long
            return formatter
        }()

        @State private var birthDate = Date()
    @State private var cap: String = ""
    @State private var cap2: String = ""
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: pdl_det_parkir_det_npwpd().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Daftar SKPD")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Text("Kost ASDF - 200.30.201.12")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontblack"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.bottom, .top],10)
                                    .padding([.leading, .trailing],20)
                                
                                HStack() {
                                    VStack() {
                                        DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date) {
                                        }
                                        .multilineTextAlignment(.leading)
                                        .frame(width: 127, height: 40)
////                                        .frame(maxWidth: .infinity, alignment: .leading)
////                                              .padding(15)
////                                            .foregroundColor(Color.gray)
//                                        .cornerRadius(15)
//                                        .overlay(
//                                            RoundedRectangle(cornerRadius: 15)
//                                                .stroke(Color("fontgray"), lineWidth: 1)
//                                        )
                                    }
                                    VStack() {
                                        DisclosureGroup("Select", isExpanded: $isExpanded) {
                                            VStack() {
                                                ForEach(1...5, id: \.self) {
                                                    num in
                                                    Text("\(num)")
                                                        .font(.system(size: 15))
                                                        .padding(.all)
                                                }
                                            }
                                        }.accentColor(Color("fontblack"))
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .padding(.all)
                                            .cornerRadius(15)
                                            .frame(width: .infinity, height: 40, alignment: .center)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Image(uiImage: #imageLiteral(resourceName: "iconFilter"))
                                        .frame(alignment: .trailing)
                                        .frame(width: 40, height: 40, alignment: .center)
//                                            .padding([.bottom],5)
                                }.padding([.leading, .trailing],20)
                                ScrollView(.vertical){
                                    NavigationLink(
                                        destination: pdl_det_parkir_daftarskpd().navigationBarHidden(true)) {
                                            HStack() {
                                                VStack(){
                                                    Text("No. SKPD: A-11234")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Masa Pajak: Juli 2022")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Jumlah Pajak: Rp. 10.000.000")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Tanggal Terbit: 2 Agustus 2022")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                                VStack(){
                                                    Button("Jatuh Tempo"){
                                                        
                                                    }
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontdanger"))
                                                    .padding()
                                                    .frame(maxWidth: .infinity)
                                                    .background(Color("fontdangersoft"))
                                                    .cornerRadius(25)
//                                                    .overlay(
//                                                            RoundedRectangle(cornerRadius: 25)
//                                                                .stroke(Color("PrimaryColor"), lineWidth: 1)
//                                                        )
                                                    .padding(.trailing, 10)
                                                }.frame(maxWidth: .infinity, alignment: .trailing)
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                    
                                    NavigationLink(
                                        destination: pdl_det_parkir_daftarskpd().navigationBarHidden(true)){
                                        HStack() {
                                            VStack(){
                                                Text("No. SKPD: A-11234")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .padding([.leading],10)
                                                    .multilineTextAlignment(.leading)
                                                Text("Masa Pajak: Juli 2022")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontgray"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .padding([.leading],10)
                                                    .multilineTextAlignment(.leading)
                                                Text("Jumlah Pajak: Rp. 10.000.000")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontgray"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .padding([.leading],10)
                                                    .multilineTextAlignment(.leading)
                                                Text("Tanggal Terbit: 2 Agustus 2022")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontgray"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .padding([.leading],10)
                                                    .multilineTextAlignment(.leading)
                                            }.frame(maxWidth: .infinity, alignment: .leading)
                                            VStack(){
                                                Button("Lunas"){
                                                    
                                                }
                                                .font(.system(size: 12))
                                                .foregroundColor(Color("fontsuccess"))
                                                .padding()
                                                .frame(maxWidth: .infinity)
                                                .background(Color("fontsuccesssoft"))
                                                .cornerRadius(25)
//                                                    .overlay(
//                                                            RoundedRectangle(cornerRadius: 25)
//                                                                .stroke(Color("PrimaryColor"), lineWidth: 1)
//                                                        )
                                                .padding(.trailing, 10)
                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                        }
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 120)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    ).padding([.leading, .trailing],20)
                                }
                                Spacer()
                            }
                        }
                    }
                }
        }
    }
}

struct pdl_det_parkir_daftarskpd_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_parkir_daftarskpd()
    }
}
