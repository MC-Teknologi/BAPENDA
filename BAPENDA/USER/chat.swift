//
//  chat.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI

struct chat: View {
    @State private var textfield: String = ""
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
//                                NavigationLink(destination: home().navigationBarHidden(true)) {
//                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
//                                        .frame(alignment: .leading)
//                                        .padding([.bottom],5)
//                                }
                                
                                Text("Customer Service")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack(){
                                NavigationLink(
                                    destination: {}) {
                                            VStack(){
                                                Text("Silahkan tuliskan pertanyaan anda pada kolom chat yang tersedia dibawah. Tim Bapenda akan membalas pertanyaan yang anda ajukan sesuai jam kerja")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontwhite"))
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .padding([.leading],10)
                                                    .padding()
                                                    .multilineTextAlignment(.leading)
                                            }.frame(alignment: .center)
                                    }
                                    .background(Color("fontgray"))
                                    .cornerRadius(10)
                                    .padding([.top], 20)
                                    .padding([.bottom], 5)
                                    .padding([.leading, .trailing], 50)
                                NavigationLink(
                                    destination: {}) {
                                            VStack(){
                                                Text("Hari ini")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontwhite"))
                                                    .frame(maxWidth: .infinity, alignment: .center)
                                                    .padding([.leading],10)
                                                    .padding()
                                                    .multilineTextAlignment(.center)
                                            }.frame(alignment: .center)
                                    }
                                    .background(Color("fontgray"))
                                    .cornerRadius(10)
                                    .padding([.leading, .trailing], 100)
                                Spacer()
                                ZStack() {
                                    
                                    Image(uiImage: #imageLiteral(resourceName: "square"))
                                        .resizable()
                                        .edgesIgnoringSafeArea(.all)
                                        .frame(height: 80)
                                    
                                    HStack{
                                        Image(uiImage: #imageLiteral(resourceName: "attach"))
                                            .resizable()
                                            .frame(width: 30, height: 30)
                                            .padding()
                                        
                                        TextField("Keterangan", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .frame(width: .infinity)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                            
                                        
                                        Image(uiImage: #imageLiteral(resourceName: "send"))
                                            .resizable()
                                            .frame(width: 30, height: 30)
                                            .padding()
                                    }
                                }.frame(width: .infinity, height: 80)
                            }
                        }
                    }
                }
        }
    }
}

struct chat_Previews: PreviewProvider {
    static var previews: some View {
        chat()
    }
}
