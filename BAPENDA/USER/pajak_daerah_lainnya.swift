//
//  pajak_daerah_lainnya.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 24/11/22.
//

import SwiftUI

struct pajak_daerah_lainnya: View {
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: home().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Pajak Daerah Lainnya")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                HStack() {
                                    NavigationLink(
                                        destination: pdl_det_hotel().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconHotel"))
                                                Text("Hotel")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: pdl_det_resto().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconResto"))
                                                Text("Resto")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: pdl_det_reklame().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconReklame"))
                                                Text("Reklame")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                }
                                .padding([.leading, .trailing],20)
                                .padding([.top],20)
                                HStack() {
                                    NavigationLink(
                                        destination: pdl_det_airtanah().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconAirtanah"))
                                                Text("Air Tanah")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    NavigationLink(
                                        destination: pdl_det_hiburan().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconHiburan"))
                                                Text("Hiburan")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    NavigationLink(
                                        destination: pdl_det_peneranganjalan().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconJalan"))
                                                Text("Penerangan Jalan")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                }.padding([.leading, .trailing],20)
                                HStack() {
                                    NavigationLink(
                                        destination: pdl_det_parkir().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconParkir"))
                                                Text("Parkir")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    NavigationLink(
                                        destination: {}) {
                                            VStack() {
//                                                Image(uiImage: #imageLiteral(resourceName: ""))
                                                Text("")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                    
                                    NavigationLink(
                                        destination: {}) {
                                            VStack() {
//                                                Image(uiImage: #imageLiteral(resourceName: ""))
                                                Text("")
                                                    .font(.system(size: 17))
                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                    
                                }.padding([.leading, .trailing],20)
                                Spacer()
                                
                            }
                        }
                    }
                }
        }
    }
}

struct pajak_daerah_lainnya_Previews: PreviewProvider {
    static var previews: some View {
        pajak_daerah_lainnya()
    }
}
