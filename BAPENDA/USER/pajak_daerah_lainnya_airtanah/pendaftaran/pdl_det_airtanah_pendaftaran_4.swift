//
//  pdl_det_airtanah_pendaftaran_4.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI
import StepperView

struct pdl_det_airtanah_pendaftaran_4: View {
    @State private var textfield: String = ""
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State private var showingCredits = false
    @State private var showingCredits2 = false
    @State var isChecked:Bool = false

    
    let steps = [ Text("Pemilik NPWPD").font(.caption).foregroundColor(Color.black),
                  Text("Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Detail Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Lampiran").font(.caption).foregroundColor(Color.black),
                  Text("Selesai").font(.caption).foregroundColor(Color.black)
                ]

    let indicationTypes = [
                            StepperIndicationType.custom(NumberedCircleView(text: "1", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "2", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "3", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "4", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "5", color: Color.white))
                        ]
    

    
        
    func toggle(){isChecked = !isChecked}
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
//                    Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                        .resizable()
//                        .frame(width: .infinity, height: 200, alignment: .center)
//                        .edgesIgnoringSafeArea(.all)
                    
                    VStack() {
                        ZStack() {
                            
//                            Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                                .resizable()
//                                .frame(width: .infinity, height: 50)
//                                .edgesIgnoringSafeArea(.all)
                            VStack(){
                                HStack(){
                                    NavigationLink(destination: pdl_det_airtanah_pendaftaran_3().navigationBarHidden(true)) {
                                        Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                            .frame(alignment: .leading)
                                            .padding([.bottom],5)
                                    }
                                    
                                    Text("Pendaftaran NPWPD")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontwhite"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],5)
                                }
                            }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        }.padding(.bottom, 10)
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Spacer()
                                StepperView()
                                        .addSteps(steps)
                                        .indicators(indicationTypes)
                                        .stepIndicatorMode(StepperMode.horizontal)
                                        .spacing(50)
                                        .lineOptions(StepperLineOptions.custom(1, Color("PrimaryColor")))
                                        .stepLifeCycles([StepLifeCycle.completed, .pending, .pending, .pending, .pending])
                                        .padding(.top, 50)
                                ScrollView(.vertical){
                                    HStack{
                                        Text("Foto KTP")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],10)
                                        Text("*")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color.red)
                                            .multilineTextAlignment(.leading)
                                            .padding([.top],10)
                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                    Divider()
                                        .padding([.leading, .trailing])
                                        .padding([.bottom, .top],5)
                                    NavigationLink(
                                        destination: pdl_det_airtanah_pendaftaran_4().navigationBarHidden(true),
                                        label: {
                                            Text("Unggah")
                                                .font(.title3)
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("PrimaryColor"))
                                                .padding()
                                                .frame(maxWidth: .infinity)
                                                .background(Color("BgColor"))
                                                .cornerRadius(10)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                )
                                            
                                        })
                                    .navigationBarHidden(true)
                                    .padding([.leading, .trailing, .bottom],10)
                                    .padding([.bottom],10)
                                    HStack{
                                        Text("Foto Objek Pajak")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],10)
                                        Text("*")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color.red)
                                            .multilineTextAlignment(.leading)
                                            .padding([.top],10)
                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                    Divider()
                                        .padding([.leading, .trailing])
                                        .padding([.bottom, .top],5)
                                    NavigationLink(
                                        destination: pdl_det_airtanah_pendaftaran_4().navigationBarHidden(true),
                                        label: {
                                            Text("Unggah")
                                                .font(.title3)
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("PrimaryColor"))
                                                .padding()
                                                .frame(maxWidth: .infinity)
                                                .background(Color("BgColor"))
                                                .cornerRadius(10)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                )
                                            
                                        })
                                    .navigationBarHidden(true)
                                    .padding([.leading, .trailing, .bottom],10)
                                    .padding([.bottom],10)
                                    HStack{
                                        Text("Dokumen Lainnya")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],10)
                                        Text("*")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color.red)
                                            .multilineTextAlignment(.leading)
                                            .padding([.top],10)
                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                    Divider()
                                        .padding([.leading, .trailing])
                                        .padding([.bottom, .top],5)
                                    NavigationLink(
                                        destination: pdl_det_airtanah_pendaftaran_4().navigationBarHidden(true),
                                        label: {
                                            Text("Unggah")
                                                .font(.title3)
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("PrimaryColor"))
                                                .padding()
                                                .frame(maxWidth: .infinity)
                                                .background(Color("BgColor"))
                                                .cornerRadius(10)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                )
                                            
                                        })
                                    .navigationBarHidden(true)
                                    .padding([.leading, .trailing, .bottom],10)
                                    .padding([.bottom],10)
                                }
                                Spacer()
                                Button(action: toggle){
                                    HStack{
                                        Image(systemName: isChecked ? "checkmark.square": "square")
                                        Text("Saya menyatakan bahwa informasi yang telah saya berikan dalam formulir ini termasuk lampirannya adalah benar, jelas dan lengkap menurut keadaan yang sebenarnya")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                    }.padding().multilineTextAlignment(.leading)
                                }
                                NavigationLink(
                                    destination: pdl_det_airtanah_pendaftaran_41().navigationBarHidden(true),
                                    label: {
                                        Text("Selanjutnya")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("BgColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color("PrimaryColor"))
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                                            )
                                        
                                    })
                                .navigationBarHidden(true)
                                .padding([.leading, .trailing, .bottom],10)
                                .padding([.bottom],10)
                        }
                    }
                }
            }
        }
    }
}

struct pdl_det_airtanah_pendaftaran_4_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_airtanah_pendaftaran_4()
    }
}
