//
//  pdl_det_airtanah_pnd_perorangan.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI

struct pdl_det_airtanah_pnd_perorangan: View {
    @State private var textfield: String = ""
    @State private var nikn: String = ""
    @State private var nikn2: String = ""
    
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    var body: some View {
        VStack(){
            ScrollView(.vertical){
                Group{
                    HStack{
                        Text("NIK")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                        Text("*")
                            .font(.system(size: 15))
                            .foregroundColor(Color.red)
                            .multilineTextAlignment(.leading)
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("NIK", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                }
                Group{
                    HStack{
                        Text("Nama Lengkap")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                        Text("*")
                            .font(.system(size: 15))
                            .foregroundColor(Color.red)
                            .multilineTextAlignment(.leading)
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("Nama Lengkap", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                    
                }
                Group{
                    HStack{
                        Text("NPWP")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                        Text("*")
                            .font(.system(size: 15))
                            .foregroundColor(Color.red)
                            .multilineTextAlignment(.leading)
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("NPWP", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                    
                }
                Group{
                    HStack{
                        Text("Kota")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                        Text("*")
                            .font(.system(size: 15))
                            .foregroundColor(Color.red)
                            .multilineTextAlignment(.leading)
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    DisclosureGroup("Kota", isExpanded: $kota) {
                        VStack() {
                            ForEach(1...5, id: \.self) {
                                num in
                                Text("\(num)")
                                    .font(.system(size: 15))
                                    .padding(.all)
                            }
                        }
                    }.accentColor(Color("fontblack"))
                        .font(.system(size: 15))
                        .foregroundColor(Color("fontblack"))
                        .padding(.all)
                        .cornerRadius(10)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                    
                }
                Group{
                    HStack{
                        Text("Kelurahan")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                        Text("*")
                            .font(.system(size: 15))
                            .foregroundColor(Color.red)
                            .multilineTextAlignment(.leading)
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    DisclosureGroup("Kelurahan", isExpanded: $kelurahan) {
                        VStack() {
                            ForEach(1...5, id: \.self) {
                                num in
                                Text("\(num)")
                                    .font(.system(size: 15))
                                    .padding(.all)
                            }
                        }
                    }.accentColor(Color("fontblack"))
                        .font(.system(size: 15))
                        .foregroundColor(Color("fontblack"))
                        .padding(.all)
                        .cornerRadius(10)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                    
                }
                Group{
                    HStack{
                        Text("Jalan")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                        Text("*")
                            .font(.system(size: 15))
                            .foregroundColor(Color.red)
                            .multilineTextAlignment(.leading)
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("Jalan", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                    
                }
                Group{
                    HStack{
                        Text("No. Telp")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                        Text("*")
                            .font(.system(size: 15))
                            .foregroundColor(Color.red)
                            .multilineTextAlignment(.leading)
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("No. Telp", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                    
                }
                DisclosureGroup("Data Narahubung", isExpanded: $narahubung2) {
                
                    Group{
                        HStack{
                            Text("NIK")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .multilineTextAlignment(.leading)
                                .padding([.leading])
                                .padding([.top],10)
                            Text("*")
                                .font(.system(size: 15))
                                .foregroundColor(Color.red)
                                .multilineTextAlignment(.leading)
                                .padding([.top],10)
                        }.frame(maxWidth: .infinity, alignment: .leading)
                        
                        
                        TextField("NIK", text: $textfield)
                            .padding(10)
                            .foregroundColor(Color.gray)
                            .cornerRadius(15)
                            .overlay(
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color("fontblack"), lineWidth: 1)
                            )
                            .padding([.leading, .trailing],10)
                        
                        HStack{
                            Text("Nama Lengkap")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .multilineTextAlignment(.leading)
                                .padding([.leading])
                                .padding([.top],10)
                            Text("*")
                                .font(.system(size: 15))
                                .foregroundColor(Color.red)
                                .multilineTextAlignment(.leading)
                                .padding([.top],10)
                        }.frame(maxWidth: .infinity, alignment: .leading)
                    
                            TextField("Nama Lengkap", text: $textfield)
                                .padding(10)
                                .foregroundColor(Color.gray)
                                .cornerRadius(15)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color("fontblack"), lineWidth: 1)
                                )
                                .padding([.leading, .trailing],10)
                    }
                    Group{

                            HStack{
                                Text("Kota")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.top],10)
                                Text("*")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color.red)
                                    .multilineTextAlignment(.leading)
                                    .padding([.top],10)
                            }.frame(maxWidth: .infinity, alignment: .leading)

                        DisclosureGroup("Kota", isExpanded: $kota2) {
                            VStack() {
                                ForEach(1...5, id: \.self) {
                                    num in
                                    Text("\(num)")
                                        .font(.system(size: 15))
                                        .padding(.all)
                                }
                            }
                        }.accentColor(Color("fontblack"))
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .padding(.all)
                            .cornerRadius(10)
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color("fontblack"), lineWidth: 1)
                            )
                            .padding([.leading, .trailing],10)
                    }
                    Group{
                            HStack{
                                Text("Kelurahan")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.top],10)
                                Text("*")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color.red)
                                    .multilineTextAlignment(.leading)
                                    .padding([.top],10)
                            }.frame(maxWidth: .infinity, alignment: .leading)

                        DisclosureGroup("Kelurahan", isExpanded: $kelurahan2) {
                            VStack() {
                                ForEach(1...5, id: \.self) {
                                    num in
                                    Text("\(num)")
                                        .font(.system(size: 15))
                                        .padding(.all)
                                }
                            }
                        }.accentColor(Color("fontblack"))
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .padding(.all)
                            .cornerRadius(10)
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color("fontblack"), lineWidth: 1)
                            )
                            .padding([.leading, .trailing],10)
                    }
                    Group{
                            HStack{
                                Text("Jalan")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.top],10)
                                Text("*")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color.red)
                                    .multilineTextAlignment(.leading)
                                    .padding([.top],10)
                            }.frame(maxWidth: .infinity, alignment: .leading)

                            TextField("Jalan", text: $textfield)
                                .padding(10)
                                .foregroundColor(Color.gray)
                                .cornerRadius(15)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color("fontblack"), lineWidth: 1)
                                )
                                .padding([.leading, .trailing],10)
                    }
                    Group{
                            HStack{
                                Text("No. Telp")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.top],10)
                                Text("*")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color.red)
                                    .multilineTextAlignment(.leading)
                                    .padding([.top],10)
                            }.frame(maxWidth: .infinity, alignment: .leading)

                            TextField("No. Telp", text: $textfield)
                                .padding(10)
                                .foregroundColor(Color.gray)
                                .cornerRadius(15)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 15)
                                        .stroke(Color("fontblack"), lineWidth: 1)
                                )
                                .padding([.leading, .trailing],10)
                    }
                    Group{
                        HStack{
                            Text("Email")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .multilineTextAlignment(.leading)
                                .padding([.leading])
                                .padding([.top],10)
                            Text("*")
                                .font(.system(size: 15))
                                .foregroundColor(Color.red)
                                .multilineTextAlignment(.leading)
                                .padding([.top],10)
                        }.frame(maxWidth: .infinity, alignment: .leading)
                        
                        TextField("Email", text: $textfield)
                            .padding(10)
                            .foregroundColor(Color.gray)
                            .cornerRadius(15)
                            .overlay(
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color("fontblack"), lineWidth: 1)
                            )
                            .padding([.leading, .trailing],10)
                    }
                    }.padding([.top, .bottom],15)
                        .padding([.leading, .trailing],5)
                        .overlay(VStack{Divider().offset(x: 0, y: .infinity)})
                        .accentColor(Color("fontblack"))
                        .font(.system(size: 15))
                        .foregroundColor(Color("fontblack"))
                
                Spacer()
                NavigationLink(
                    destination: pdl_det_airtanah_pendaftaran_2().navigationBarHidden(true),
                    label: {
                        Text("Selanjutnya")
                            .font(.title3)
                            .fontWeight(.bold)
                            .foregroundColor(Color("BgColor"))
                            .padding()
                            .frame(maxWidth: .infinity)
                            .background(Color("PrimaryColor"))
                            .cornerRadius(10)
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                            )
                        
                    })
                .navigationBarHidden(true)
                .padding([.leading, .trailing, .bottom],10)
                .padding([.bottom],10)
            }
        }
    }
}

struct pdl_det_airtanah_pnd_perorangan_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_airtanah_pnd_perorangan()
    }
}
