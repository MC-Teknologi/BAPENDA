//
//  pdl_det_airtanah_nothing.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 28/11/22.
//

import SwiftUI

struct pdl_det_airtanah_nothing: View {
    @State private var showingCredits = false
    @State private var showingCredits2 = false
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var cap: String = ""
    @State private var cap2: String = ""
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: pajak_daerah_lainnya().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Pajak airtanah")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                HStack() {
                                    NavigationLink(
                                        destination: pdl_det_airtanah().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Pendaftaran NPWPD")
                                                    .font(.system(size: 15))
                                                //                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: welcome_page().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Lapor E-SPTPD")
                                                    .font(.system(size: 15))
                                                //                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: welcome_page().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Pengajuan")
                                                    .font(.system(size: 17))
                                                //                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                }.padding([.leading, .trailing],20).padding([.top],20)
                                Text("Daftar NPWPD airtanah")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontblack"))
                                //                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom, .top],10)
                                
                                ScrollView(.vertical){
                                    Spacer()
//                                    NavigationLink(destination: register_data_akun2().navigationBarHidden(true)) {
                                        Image(uiImage: #imageLiteral(resourceName: "iconNothing"))
                                            .frame(maxWidth: .infinity, alignment: .center)
                                            .padding([.bottom],5)
                                            .padding([.top],15)
//                                    }
                                    
                                    Text("Daftar NPWPD Belum Ada")
                                        .font(.system(size: 25))
                                        .foregroundColor(Color("fontblack"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],10)
                                    
                                    
                                    HStack() {
                                        
                                        Text("Sudah memiliki NPWPD?")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                        
                                        Text("Silahkan")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                        
                                        Button("Verifikasi") {
                                            showingCredits.toggle()
                                                
                                        }
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("PrimaryColor"))
                                        .sheet(isPresented: $showingCredits) {
                                            ZStack {
                                                Color("BgColor").edgesIgnoringSafeArea(.all)
                                                VStack() {
                                                    Text("Pendaftaran NPWPD")
                                                        .font(.system(size: 20))
                                                        .fontWeight(.bold)
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .multilineTextAlignment(.leading)
                                                        .padding([.leading])
                                                        .padding([.top, .bottom],20)
                                                    
                                                    Text("NPWPD")
                                                        .font(.system(size: 15))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .multilineTextAlignment(.leading)
                                                        .padding([.leading])
                                                        .padding([.top],20)
                                                    
                                                    TextField("NPWPD", text: $username)
                                                        .padding(15)
                                                        .foregroundColor(Color.gray)
                                                        .cornerRadius(15)
                                                        .overlay(
                                                            RoundedRectangle(cornerRadius: 15)
                                                                .stroke(Color("fontblack"), lineWidth: 1)
                                                        )
                                                        .padding([.leading, .trailing],10)
                                                        .padding([.bottom],10)
                                                    
                                                    NavigationLink(
                                                        destination: pdl_det_airtanah_nothing().navigationBarHidden(true),
                                                        label: {
                                                            Text("Unggah foto NPWPD")
                                                                .font(.title3)
                                                                .fontWeight(.bold)
                                                                .foregroundColor(Color("PrimaryColor"))
                                                                .padding()
                                                                .frame(maxWidth: .infinity)
                                                                .background(Color("BgColor"))
                                                                .cornerRadius(10)
                                                                .overlay(
                                                                        RoundedRectangle(cornerRadius: 10)
                                                                            .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                                    )
                                                                
                                                        })
                                                        .navigationBarHidden(true)
                                                        .padding([.leading, .trailing],10)
                                                    Spacer()
                                                    Button("Cari NPWPD") {
                                                            
                                                        showingCredits2.toggle()
                                                    }
                                                        .font(.title3)
                                                        .foregroundColor(Color("fontwhite"))
                                                        .padding()
                                                        .frame(maxWidth: .infinity)
                                                        .background(Color("PrimaryColor"))
                                                        .cornerRadius(10)
                                                        .overlay(
                                                                RoundedRectangle(cornerRadius: 10)
                                                                    .stroke(Color("BgColor"), lineWidth: 1)
                                                            )
                                                        .navigationBarHidden(true)
                                                        .padding([.leading, .trailing, .bottom],10)
                                                        .sheet(isPresented: $showingCredits2) {
                                                            ZStack {
                                                                Color("BgColor").edgesIgnoringSafeArea(.all)
                                                                VStack() {
                                                                    Text("Verifikasi NPWPD")
                                                                        .font(.system(size: 20))
                                                                        .fontWeight(.bold)
                                                                        .foregroundColor(Color("fontblack"))
                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                        .multilineTextAlignment(.leading)
                                                                        .padding([.leading])
                                                                        .padding([.top, .bottom],20)
                                                                   
                                                                    NavigationLink(
                                                                        destination: pdl_det_airtanah_nothing().navigationBarHidden(true)) {
                                                                            HStack() {
                                                                                VStack(){
                                                                                    Text("NPWPD")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontblack"))
                                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                                        .padding([.leading],10)
                                                                                        .multilineTextAlignment(.leading)
                                                                                    Text("Nama Usaha")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontgray"))
                                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                                        .padding([.leading],10)
                                                                                        .multilineTextAlignment(.leading)
                                                                                    Text("Nama Pemilik")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontgray"))
                                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                                        .padding([.leading],10)
                                                                                        .multilineTextAlignment(.leading)
                                                                                    Text("Alamat")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontgray"))
                                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                                        .padding([.leading],10)
                                                                                        .multilineTextAlignment(.leading)
                                                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                                                                VStack(){
                                                                                    
                                                                                    Text("02**.**.700")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontblack"))
                                                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                                                        .padding([.trailing],10)
                                                                                        .multilineTextAlignment(.center)
                                                                                    Text("airtanah **** ***GH")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontblack"))
                                                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                                                        .padding([.trailing],10)
                                                                                        .multilineTextAlignment(.center)
                                                                                    Text("N**a ***** **ak")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontblack"))
                                                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                                                        .padding([.trailing],10)
                                                                                        .multilineTextAlignment(.center)
                                                                                    Text("Ala*** **** ***ak")
                                                                                        .font(.system(size: 12))
                                                                                        .foregroundColor(Color("fontblack"))
                                                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                                                        .padding([.trailing],10)
                                                                                        .multilineTextAlignment(.center)
                                                                                }.frame(maxWidth: .infinity, alignment: .trailing)
                                                                            }
                                                                        }
                                                                        .frame(maxWidth: .infinity, minHeight: 120)
                                                                        .background(Color("BgColor"))
                                                                        .cornerRadius(10)
                                                                        .shadow(
                                                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                                                        ).padding([.leading, .trailing], 20)
//                                                                        .shadow(
//                                                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
//                                                                        ).padding([.leading, .trailing],20))
                                                                    Spacer()
                                                                    NavigationLink(
                                                                        destination: pdl_det_airtanah_notingsuccess(),
                                                                        label: {
                                                                            Text("Verifikasi")
                                                                                .font(.title3)
                                                                                .fontWeight(.bold)
                                                                                .foregroundColor(Color("BgColor"))
                                                                                .padding()
                                                                                .frame(maxWidth: .infinity)
                                                                                .background(Color("PrimaryColor"))
                                                                                .cornerRadius(10)
                                                                                .overlay(
                                                                                    RoundedRectangle(cornerRadius: 10)
                                                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                                                )

                                                                        })
                                                                    .navigationBarHidden(true)
                                                                    .padding([.leading, .trailing, .bottom],10)
                                                                    .padding([.bottom],10)
                                                                }
                                                            }
                                                        }
                                                        
                                                }
                                            }
                                        }
                                    }
                                    
                                    Spacer()
                                    
                                }
                                
                            }
                            
                        }
                    }
                }
        }
    }
}

struct pdl_det_airtanah_nothing_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_hotel_nothing()
    }
}
