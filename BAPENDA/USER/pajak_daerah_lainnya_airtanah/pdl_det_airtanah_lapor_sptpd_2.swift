//
//  pdl_det_airtanah_lapor_sptpd_2.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI


struct pdl_det_airtanah_lapor_sptpd_2: View {
    @State private var textfield: String = ""
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State private var showingCredits = false
    @State private var showingCredits2 = false
    @State var hours: Int = 0
    @State var minutes: Int = 0
    @State private var newEatenMealTime = Date()
    @State private var showingNewMealTime = false
    @State var isChecked:Bool = false
    func toggle(){isChecked = !isChecked}
   
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: pdl_det_airtanah().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Form Lapor E-SPTPD")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
//                                NavigationLink(
//                                    destination: pdl_det_airtanah_lapor_sptpd().navigationBarHidden(true)) {
                                        HStack(){
                                            Image(uiImage: #imageLiteral(resourceName: "card"))
                                                .padding([.leading],10)
                                            VStack{
                                                Group{
                                                    Text("123123.123.12.3")
                                                        .font(.system(size: 15))
                                                        .foregroundColor(Color("fontblack"))
                                                        .fontWeight(.bold)
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Rumah Kos")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Kos ASDFG")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .multilineTextAlignment(.leading)
                                                    Text("JL. Mayjend Sungkono No.32A ,005/004, ")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Sukun, Sukun")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .multilineTextAlignment(.leading)
                                                }
                                            }
                                            Image(uiImage: #imageLiteral(resourceName: "next"))
                                                .padding([.trailing],10)
                                        }
//                                    }
//                                    .frame(maxWidth: .infinity, minHeight: 120)
//                                    .background(Color("BgColor"))
//                                    .cornerRadius(10)
//                                    .shadow(
//                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
//                                        ).padding([.leading, .trailing],20)
                                    .padding(.top, 20)
                                    .padding([.leading, .trailing], 10)
                                Divider()
                                    .padding([.leading, .trailing])
                                    .padding([.bottom, .top],5)
                                
                                ScrollView(.vertical){
                                    Group{
                                        Text("Masa Pajak : Bulah Tahun")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .fontWeight(.bold)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],10)
                                        
                                        Text("Detail Objek Pajak")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .fontWeight(.bold)
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],5)
                                        }
                                    NavigationLink(
                                        destination: pdl_det_airtanah_det_npwpd().navigationBarHidden(true)) {
                                            VStack(){
                                                Text("Tarif dan Jumlah Kamar airtanah")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .fontWeight(.bold)
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .multilineTextAlignment(.leading)
                                                    .padding([.leading])
                                                    .padding([.bottom, .top],5)
                                                pdl_det_airtanah_lapor_sptpd_isi()
                                                Button("Tambah Jenis Kamar") {
                                                    
                                                    showingCredits2.toggle()
                                                }
                                                .font(.title3)
                                                .foregroundColor(Color("fontwhite"))
                                                .padding()
                                                .frame(maxWidth: .infinity)
                                                .background(Color("PrimaryColor"))
                                                .cornerRadius(10)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("BgColor"), lineWidth: 1)
                                                )
                                                .navigationBarHidden(true)
                                                .padding([.leading, .trailing, .bottom],10)
                                                .sheet(isPresented: $showingCredits2) {
                                                    if #available(iOS 16, *) {
                                                        pdl_det_airtanah_lapor_sptpdpresentation2()
                                                        .presentationDetents([.medium, .fraction(0.7)])
                                                    }else{
                                                        pdl_det_airtanah_lapor_sptpdpresentation2()
                                                    }
                                                    
                                                }
                                                
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                    Spacer()
                                    pdl_det_airtanah_lapor_sptpd22()
                                    NavigationLink(
                                        destination: pdl_det_airtanah_lapor_sptpd_2().navigationBarHidden(true),
                                        label: {
                                            Text("Lampirakan Pendapatan")
                                                .font(.title3)
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("PrimaryColor"))
                                                .padding()
                                                .frame(maxWidth: .infinity)
                                                .background(Color("BgColor"))
                                                .cornerRadius(10)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                )

                                        })
                                    .navigationBarHidden(true)
                                    .padding([.leading, .trailing],10)
                                        
                                    Button(action:{self.showingNewMealTime = true})
                                    {
                                        Text("Submit")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("BgColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color("PrimaryColor"))
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                                            )
                                        }.navigationBarHidden(true)
                                        .padding([.leading, .trailing],10)
                                        .padding([.bottom],20)
                                   
                            }
                        }
                    }
                }
                    
                    if $showingNewMealTime.wrappedValue {
                        VStack(alignment: .center) {
                            ZStack{
                                Color.black.opacity(0.4)
                                    .edgesIgnoringSafeArea(.vertical)
                                // this one is it
                                VStack(spacing: 20) {
//                                    Text("Time between meals")
//                                        .bold().padding()
//                                        .frame(maxWidth: .infinity)
//                                        .background(Color("PrimaryColor"))
//                                        .foregroundColor(Color.white)
                                    
                                    Image(uiImage: #imageLiteral(resourceName: "IconCheck")).padding(.top, 20)
                                    
                                    Text("Selesai!")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontblack"))
                                        .frame(width: .infinity)
                                        .padding([.top], 10)
                                    
                                    Text("Pengajuan anda akan diverifikasi oleh petugas")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("fontblack"))
                                        .frame(width: .infinity)
                                        .padding([.top], 10)
                                    
                                    Spacer()
                                    Button(action: {
                                        self.showingNewMealTime = false
                                    }){
                                        Text("Halaman Utama")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("BgColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color("PrimaryColor"))
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                                            )
                                        }.navigationBarHidden(true)
                                        .padding([.leading, .trailing],10)
                                        .padding([.bottom],20)
                                }
                                .frame(width:300, height: 300)
                                .background(Color.white)
                                .mask(RoundedRectangle(cornerRadius: 20))
                                .shadow(radius: 20)
                            }
                        }
                    }
            }
        }
    }
}

struct pdl_det_airtanah_lapor_sptpd_2_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_airtanah_lapor_sptpd_2()
    }
}
struct pdl_det_airtanah_lapor_sptpdpresentation2: View {
    @State private var textfield: String = ""
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    var body: some View {
        ZStack {
            Color("BgColor").edgesIgnoringSafeArea(.all)
            VStack() {
                
                Group{
                    HStack{
                        Text("Jenis Kamar")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("Jenis Kamar", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                }
                Group{
                    HStack{
                        Text("Jumlah Kamar")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("Jumlah Kamar", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                }
                Group{
                    HStack{
                        Text("Tarif Kamar")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("Tarif Kamar", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                }
                Group{
                    HStack{
                        Text("Kapasitas Kamar")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.top],10)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    
                    TextField("kapasitas Kamar", text: $textfield)
                        .padding(10)
                        .foregroundColor(Color.gray)
                        .cornerRadius(15)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("fontblack"), lineWidth: 1)
                        )
                        .padding([.leading, .trailing],10)
                }
                Spacer()
                
                NavigationLink(
                    destination: pdl_det_airtanah_pendaftaran_31().navigationBarHidden(true),
                    label: {
                        Text("Tambahkan")
                            .font(.title3)
                            .fontWeight(.bold)
                            .foregroundColor(Color("BgColor"))
                            .padding()
                            .frame(maxWidth: .infinity)
                            .background(Color("PrimaryColor"))
                            .cornerRadius(10)
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                            )
                        
                    })
                .navigationBarHidden(true)
                .padding([.leading, .trailing, .bottom],10)
                .padding([.bottom],10)
            }.padding(.top, 20)
        }
    }
}
struct pdl_det_airtanah_lapor_sptpd22: View {
    @State private var textfield: String = ""
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State private var showingCredits = false
    @State private var showingCredits2 = false
    @State var isChecked:Bool = false
    func toggle(){isChecked = !isChecked}
    var body: some View {
        
        HStack{
            Text("Menggunakan Kas Register/Komputer/Pos")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            Text("*")
                .font(.system(size: 15))
                .foregroundColor(Color.red)
                .multilineTextAlignment(.leading)
                .padding([.top],10)
            Text(" : ")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            Button(action: toggle){
                HStack{
                    Image(systemName: isChecked ? "checkmark.square": "square")
                    Text("Ya")
                        .font(.system(size: 15))
                        .foregroundColor(Color("fontblack"))
                }.padding(.top, 10)
            }
            Button(action: toggle){
                HStack{
                    Image(systemName: isChecked ? "checkmark.square": "square")
                    Text("Tidak")
                        .font(.system(size: 15))
                        .foregroundColor(Color("fontblack"))
                }.padding(.top, 10)
            }
        }.frame(maxWidth: .infinity, alignment: .leading)
        HStack{
            Text("Mengadakan Pembukuan Pencatatan")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            Text("*")
                .font(.system(size: 15))
                .foregroundColor(Color.red)
                .multilineTextAlignment(.leading)
                .padding([.top],10)
            Text(" : ")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            Button(action: toggle){
                HStack{
                    Image(systemName: isChecked ? "checkmark.square": "square")
                    Text("Ya")
                        .font(.system(size: 15))
                        .foregroundColor(Color("fontblack"))
                }.padding(.top, 10)
            }
            Button(action: toggle){
                HStack{
                    Image(systemName: isChecked ? "checkmark.square": "square")
                    Text("Tidak")
                        .font(.system(size: 15))
                        .foregroundColor(Color("fontblack"))
                }.padding(.top, 10)
            }
        }.frame(maxWidth: .infinity, alignment: .leading)
        Divider()
            .padding([.leading, .trailing])
            .padding([.bottom, .top],5)
        HStack{
            Text("Omset (Rp)")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            Text("*")
                .font(.system(size: 15))
                .foregroundColor(Color.red)
                .multilineTextAlignment(.leading)
                .padding([.top],10)
            TextField("Omset RP", text: $textfield)
                .padding(10)
                .foregroundColor(Color.gray)
                .cornerRadius(15)
                .overlay(
                    RoundedRectangle(cornerRadius: 15)
                        .stroke(Color("fontblack"), lineWidth: 1)
                )
                .padding([.leading, .trailing],10)
        }.frame(maxWidth: .infinity, alignment: .leading)
        HStack{
            Text("Tarif Pajak")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            Text("5%")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.trailing)
                .frame(width: .infinity, alignment: .trailing)
                .padding([.trailing])
                .padding([.top],10)
            
        }.frame(maxWidth: .infinity, alignment: .leading)
        HStack{
            Text("Jumlah Pajak")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            Text("Rp. xx.xxx.xxx")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.trailing)
                .frame(width: .infinity, alignment: .trailing)
                .padding([.leading])
                .padding([.top],10)
            
        }.frame(maxWidth: .infinity, alignment: .leading).padding(.bottom, 20)
        HStack{
            Text("Unduh Format Lampiran Pendapatan")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
            NavigationLink(destination: register().navigationBarHidden(true)) {
                Text("Disini")
                    .font(.system(size: 15))
                    .foregroundColor(Color("PrimaryColor"))
            }
            .navigationBarHidden(true)
            .padding([.leading, .trailing],10)
            .padding([.top],10)
            
        }.frame(maxWidth: .infinity, alignment: .leading)
        
    }
}

struct pdl_det_airtanah_lapor_sptpd_isi: View {
    var body: some View {
        VStack{
            Text("Deluxe")
                .font(.system(size: 15))
                .foregroundColor(Color("fontblack"))
                .multilineTextAlignment(.leading)
                .padding([.leading])
                .padding([.top],10)
        }.frame(maxWidth: .infinity, alignment: .leading)
        HStack{
            VStack{
                Text("Jumlah Kamar")
                    .font(.system(size: 15))
                    .foregroundColor(Color("fontblack"))
                    .multilineTextAlignment(.leading)
                    .padding([.leading])
                    .padding([.top],10)
                Text("Tarif")
                    .font(.system(size: 15))
                    .foregroundColor(Color("fontblack"))
                    .multilineTextAlignment(.leading)
                    .padding([.leading])
                    .padding([.top],5)
                Text("Jumlah Kamar Yang Laku")
                    .font(.system(size: 15))
                    .foregroundColor(Color("fontblack"))
                    .multilineTextAlignment(.leading)
                    .padding([.leading])
                    .padding([.top],5)
            }.frame(alignment: .leading)
            VStack{
                Text("5 Kamar")
                    .font(.system(size: 15))
                    .foregroundColor(Color("fontblack"))
                    .multilineTextAlignment(.trailing)
                    .padding([.trailing])
                    .padding([.top],10)
                Text("Rp. 1.000.000")
                    .font(.system(size: 15))
                    .foregroundColor(Color("fontblack"))
                    .multilineTextAlignment(.trailing)
                    .padding([.trailing])
                    .padding([.top],5)
                Text("10")
                    .font(.system(size: 15))
                    .foregroundColor(Color("fontblack"))
                    .multilineTextAlignment(.trailing)
                    .padding([.trailing])
                    .padding([.top],5)
            }.frame(maxWidth: .infinity, alignment: .trailing)
            
        }.frame(maxWidth: .infinity)
        Divider()
            .padding([.leading, .trailing])
            .padding([.bottom, .top],5)
    }
}

