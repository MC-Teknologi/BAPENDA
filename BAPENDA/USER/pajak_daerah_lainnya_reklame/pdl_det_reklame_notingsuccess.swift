//
//  pdl_det_reklame_notingsuccess.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 29/11/22.
//

import SwiftUI

struct pdl_det_reklame_notingsuccess: View {
    @State private var showingCredits = false
    @State private var showingCredits2 = false
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var cap: String = ""
    @State private var cap2: String = ""
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: pajak_daerah_lainnya().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Pajak reklame")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                HStack() {
                                    NavigationLink(
                                        destination: pdl_det_reklame().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Pendaftaran NPWPD")
                                                    .font(.system(size: 15))
                                                //                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: welcome_page().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Lapor E-SPTPD")
                                                    .font(.system(size: 15))
                                                //                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: welcome_page().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Pengajuan")
                                                    .font(.system(size: 17))
                                                //                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                }.padding([.leading, .trailing],20).padding([.top],20)
                                Text("Daftar NPWPD reklame")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontblack"))
                                //                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom, .top],10)
                                
                                ScrollView(.vertical){
                                    Spacer()
                                    //                                    NavigationLink(destination: register_data_akun2().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName:"iconCheck"))
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .padding([.bottom],5)
                                        .padding([.top],15)
                                    //                                    }
                                    Text("Verifikasi NPWPD Selesai!")
                                        .font(.system(size: 25))
                                        .foregroundColor(Color("fontblack"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],10)
                                    
                                    Text("Pengajuan anda diverifikasi oleh petugas")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontblack"))
//                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],10)
                                    
                                    Text("Daftar NPWPD Belum Ada")
                                        .font(.system(size: 25))
                                        .foregroundColor(Color("fontblack"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],10)
                                    
                                    
                                    HStack() {
                                        
                                        Text("Sudah memiliki NPWPD?")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                        
                                        Text("Silahkan")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                        
                                        Button("Verifikasi") {
                                            showingCredits.toggle()
                                            
                                        }
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("PrimaryColor"))
                                    }
                                }
                                
                                Spacer()
                                
                            }
                        }
                    }
                }
        }
    }
}

struct pdl_det_reklame_notingsuccess_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_reklame_notingsuccess()
    }
}
