//
//  pdl_det_reklame.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI

struct pdl_det_reklame: View {
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: pajak_daerah_lainnya().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Pajak reklame")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                HStack() {
                                    NavigationLink(
                                        destination: pdl_det_reklame_pendaftaran().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Pendaftaran NPWPD")
                                                    .font(.system(size: 15))
//                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: pdl_det_reklame_lapor_sptpd().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Lapor E-SPTPD")
                                                    .font(.system(size: 15))
//                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: pdl_det_reklame_pengajuan().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Pengajuan")
                                                    .font(.system(size: 17))
//                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                }.padding([.leading, .trailing],20).padding([.top],20)
                                Text("Daftar NPWPD reklame")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontblack"))
//                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom, .top],10)
                                
                                ScrollView(.vertical){
                                    NavigationLink(
                                        destination: pdl_det_reklame_det_npwpd().navigationBarHidden(true)) {
                                            HStack() {
                                                VStack(){
                                                    Text("NPWPD")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Nama Usaha")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Nama Pemilik")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Alamat")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                                VStack(){
                                                    Text("0283.62.700")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                        .padding([.trailing],10)
                                                        .multilineTextAlignment(.trailing)
                                                    Text("reklame ASDFG")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                        .padding([.trailing],10)
                                                        .multilineTextAlignment(.trailing)
                                                    Text("Nama Wajib Pajak")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                        .padding([.trailing],10)
                                                        .multilineTextAlignment(.trailing)
                                                    Text("Alamat Objek Pajak")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                        .padding([.trailing],10)
                                                        .multilineTextAlignment(.trailing)
                                                }.frame(maxWidth: .infinity, alignment: .trailing)
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                }
                                Spacer()
                                NavigationLink(
                                    destination: pdl_det_reklame_det_npwpd().navigationBarHidden(true),
                                    label: {
                                        Text("Verifikasi NPWPD reklame")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("PrimaryColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color.white)
                                            .cornerRadius(10)
                                            .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                )
                                            
                                    })
                                    .navigationBarHidden(true)
                                    .padding([.leading, .trailing],10)
                                    .padding([.bottom],20)
                            }
                        }
                    }
                }
        }
    }
}

struct pdl_det_reklame_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_reklame()
    }
}
