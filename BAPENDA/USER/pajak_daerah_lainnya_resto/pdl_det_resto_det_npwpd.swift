//
//  pdl_det_resto_det_npwpd.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 28/11/22.
//

import SwiftUI

struct pdl_det_resto_det_npwpd: View {
    @State private var isDisclosed = false
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: pdl_det_resto().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Detail NPWPD")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                HStack() {
                                    NavigationLink(
                                        destination: pdl_det_resto_daftaresptpd().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Daftar E-SPTPD")
                                                    .font(.system(size: 15))
//                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: pdl_det_resto_daftarskpd().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Daftar SKPD")
                                                    .font(.system(size: 15))
//                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                    
                                    
                                    NavigationLink(
                                        destination: pdl_det_resto_catatanpembayaran().navigationBarHidden(true)) {
                                            VStack() {
                                                Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                                Text("Catatan Pembayaran")
                                                    .font(.system(size: 17))
//                                                    .fontWeight(.bold)
                                                    .foregroundColor(Color("fontblack"))
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 130)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        )
                                }.padding([.leading, .trailing],20).padding([.top],20)
                                
                                
                                ScrollView(.vertical){
                                        
                                    NavigationLink(
                                        destination: pdl_det_resto_det_npwpd().navigationBarHidden(true)) {
                                            VStack(){
                                                Text("Informasi Objek Pajak")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .fontWeight(.bold)
                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                    .multilineTextAlignment(.leading)
                                                    .padding([.leading])
                                                    .padding([.bottom, .top],5)
                                                HStack() {
                                                    VStack(){
                                                        Text("NPWPD")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontgray"))
                                                            .frame(maxWidth: .infinity, alignment: .leading)
                                                            .padding([.leading],10)
                                                            .multilineTextAlignment(.leading)
                                                        Text("NOP")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontgray"))
                                                            .frame(maxWidth: .infinity, alignment: .leading)
                                                            .padding([.leading],10)
                                                            .multilineTextAlignment(.leading)
                                                        Text("Jenis Usaha")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontgray"))
                                                            .frame(maxWidth: .infinity, alignment: .leading)
                                                            .padding([.leading],10)
                                                            .multilineTextAlignment(.leading)
                                                        Text("Nama Objek Pajak")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontgray"))
                                                            .frame(maxWidth: .infinity, alignment: .leading)
                                                            .padding([.leading],10)
                                                            .multilineTextAlignment(.leading)
                                                        Text("No Telp")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontgray"))
                                                            .frame(maxWidth: .infinity, alignment: .leading)
                                                            .padding([.leading],10)
                                                            .multilineTextAlignment(.leading)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    VStack(){
                                                        Text("1234.50.120")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontblack"))
                                                            .frame(maxWidth: .infinity, alignment: .trailing)
                                                            .padding([.trailing],10)
                                                            .multilineTextAlignment(.trailing)
                                                        Text("35.73.050.001.001.0001.0")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontblack"))
                                                            .frame(maxWidth: .infinity, alignment: .trailing)
                                                            .padding([.trailing],10)
                                                            .multilineTextAlignment(.trailing)
                                                        Text("Rumah Kost")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontblack"))
                                                            .frame(maxWidth: .infinity, alignment: .trailing)
                                                            .padding([.trailing],10)
                                                            .multilineTextAlignment(.trailing)
                                                        Text("Rumah Kost ASDF")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontblack"))
                                                            .frame(maxWidth: .infinity, alignment: .trailing)
                                                            .padding([.trailing],10)
                                                            .multilineTextAlignment(.trailing)
                                                        Text("081234567890")
                                                            .font(.system(size: 12))
                                                            .foregroundColor(Color("fontblack"))
                                                            .frame(maxWidth: .infinity, alignment: .trailing)
                                                            .padding([.trailing],10)
                                                            .multilineTextAlignment(.trailing)
                                                    }.frame(maxWidth: .infinity, alignment: .trailing)
                                                }
                                                VStack {
                                                    HStack() {
                                                        VStack(){
                                                            Text("Alamat")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontgray"))
                                                                .frame(maxWidth: .infinity, alignment: .leading)
                                                                .padding([.leading],10)
                                                                .multilineTextAlignment(.leading)
                                                            Text("RT/RW")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontgray"))
                                                                .frame(maxWidth: .infinity, alignment: .leading)
                                                                .padding([.leading],10)
                                                                .multilineTextAlignment(.leading)
                                                            Text("Kecamatan")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontgray"))
                                                                .frame(maxWidth: .infinity, alignment: .leading)
                                                                .padding([.leading],10)
                                                                .multilineTextAlignment(.leading)
                                                            Text("Kelurahan")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontgray"))
                                                                .frame(maxWidth: .infinity, alignment: .leading)
                                                                .padding([.leading],10)
                                                                .multilineTextAlignment(.leading)
                                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                                        VStack(){
                                                            Text("Jl. Mayjend Sungkono no.3")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontblack"))
                                                                .frame(maxWidth: .infinity, alignment: .trailing)
                                                                .padding([.trailing],10)
                                                                .multilineTextAlignment(.trailing)
                                                            Text("005/002")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontblack"))
                                                                .frame(maxWidth: .infinity, alignment: .trailing)
                                                                .padding([.trailing],10)
                                                                .multilineTextAlignment(.trailing)
                                                            Text("Kedungkandang")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontblack"))
                                                                .frame(maxWidth: .infinity, alignment: .trailing)
                                                                .padding([.trailing],10)
                                                                .multilineTextAlignment(.trailing)
                                                            Text("Kedungkandang")
                                                                .font(.system(size: 12))
                                                                .foregroundColor(Color("fontblack"))
                                                                .frame(maxWidth: .infinity, alignment: .trailing)
                                                                .padding([.trailing],10)
                                                                .multilineTextAlignment(.trailing)
                                                        }.frame(maxWidth: .infinity, alignment: .trailing)
                                                    }.padding(.top,10)
                                                }
                                                .frame(height: isDisclosed ? nil : 0, alignment: .top)
                                                .clipped()
                                                .id(0)
                                                Button("Lebih Banyak") {
                                                    withAnimation {
                                                        isDisclosed.toggle()
                                                    }
                                                }
                                                .id(0)
                                                .font(.system(size: 12))
                                                .foregroundColor(Color("fontgray"))
                                                .frame(maxWidth: .infinity, alignment: .center)
                                                .padding([.top, .bottom],10)
                                                .multilineTextAlignment(.center)
                                                
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                            ).padding([.leading, .trailing],20)
                                    
                                        
                                            NavigationLink(
                                                destination: pdl_det_resto_det_npwpd().navigationBarHidden(true)) {
                                                    VStack(){
                                                        Text("Informasi Wajib Pajak")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .fontWeight(.bold)
                                                            .frame(maxWidth: .infinity, alignment: .leading)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.bottom, .top],5)
                                                        HStack() {
                                                            VStack(){
                                                                Text("Nama Wajib Pajak")
                                                                    .font(.system(size: 12))
                                                                    .foregroundColor(Color("fontgray"))
                                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                                    .padding([.leading],10)
                                                                    .multilineTextAlignment(.leading)
                                                                Text("NIK")
                                                                    .font(.system(size: 12))
                                                                    .foregroundColor(Color("fontgray"))
                                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                                    .padding([.leading],10)
                                                                    .multilineTextAlignment(.leading)
                                                                Text("No. Telp")
                                                                    .font(.system(size: 12))
                                                                    .foregroundColor(Color("fontgray"))
                                                                    .frame(maxWidth: .infinity, alignment: .leading)
                                                                    .padding([.leading],10)
                                                                    .multilineTextAlignment(.leading)
                                                            }.frame(maxWidth: .infinity, alignment: .leading)
                                                            VStack(){
                                                                Text("Indah Suryani")
                                                                    .font(.system(size: 12))
                                                                    .foregroundColor(Color("fontblack"))
                                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                                    .padding([.trailing],10)
                                                                    .multilineTextAlignment(.trailing)
                                                                Text("35731937357899902")
                                                                    .font(.system(size: 12))
                                                                    .foregroundColor(Color("fontblack"))
                                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                                    .padding([.trailing],10)
                                                                    .multilineTextAlignment(.trailing)
                                                                Text("081234567890")
                                                                    .font(.system(size: 12))
                                                                    .foregroundColor(Color("fontblack"))
                                                                    .frame(maxWidth: .infinity, alignment: .trailing)
                                                                    .padding([.trailing],10)
                                                                    .multilineTextAlignment(.trailing)
                                                            }.frame(maxWidth: .infinity, alignment: .trailing)
                                                        }
                                                        VStack {
                                                            HStack() {
                                                                VStack(){
                                                                    Text("Alamat")
                                                                        .font(.system(size: 12))
                                                                        .foregroundColor(Color("fontgray"))
                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                        .padding([.leading],10)
                                                                        .multilineTextAlignment(.leading)
                                                                    Text("RT/RW")
                                                                        .font(.system(size: 12))
                                                                        .foregroundColor(Color("fontgray"))
                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                        .padding([.leading],10)
                                                                        .multilineTextAlignment(.leading)
                                                                    Text("Kecamatan")
                                                                        .font(.system(size: 12))
                                                                        .foregroundColor(Color("fontgray"))
                                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                                        .padding([.leading],10)
                                                                        .multilineTextAlignment(.leading)
                                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                                                VStack(){
                                                                    Text("Jl. Dewandaru no.100")
                                                                        .font(.system(size: 12))
                                                                        .foregroundColor(Color("fontblack"))
                                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                                        .padding([.trailing],10)
                                                                        .multilineTextAlignment(.trailing)
                                                                    Text("003/004")
                                                                        .font(.system(size: 12))
                                                                        .foregroundColor(Color("fontblack"))
                                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                                        .padding([.trailing],10)
                                                                        .multilineTextAlignment(.trailing)
                                                                    Text("Jatimulyo")
                                                                        .font(.system(size: 12))
                                                                        .foregroundColor(Color("fontblack"))
                                                                        .frame(maxWidth: .infinity, alignment: .trailing)
                                                                        .padding([.trailing],10)
                                                                        .multilineTextAlignment(.trailing)
                                                                }.frame(maxWidth: .infinity, alignment: .trailing)
                                                            }.padding(.top,10)
                                                        }
                                                        .frame(height: isDisclosed ? nil : 0, alignment: .top)
                                                        .clipped()
                                                        .id(1)
                                                        Button("Lebih Banyak") {
                                                            withAnimation {
                                                                isDisclosed.toggle()
                                                            }
                                                        }
                                                        .id(1)
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .center)
                                                        .padding([.top, .bottom],10)
                                                        .multilineTextAlignment(.center)
                                                    }
                                                    
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                }
                                Spacer()
                                NavigationLink(
                                    destination: pdl_det_resto_det_npwpd().navigationBarHidden(true),
                                    label: {
                                        Text("Verifikasi NPWPD Resto")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("PrimaryColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color.white)
                                            .cornerRadius(10)
                                            .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                )
                                            
                                    })
                                    .navigationBarHidden(true)
                                    .padding([.leading, .trailing],10)
                                    .padding([.bottom],20)
                            }
                        }
                    }
                }
        }
    }
}

struct pdl_det_resto_det_npwpd_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_resto_det_npwpd()
    }
}

struct Detail2: View {
    @State var isExpanded :Bool
    var body: some View {
        ZStack {
            ZStack {
                RoundedRectangle(cornerRadius: 8)
                .fill(Color(red: 0.0, green: 1.0, blue: 1.0, opacity: 1.0)).frame(height: 115)
                Text("Hello, World!")
            }.zIndex(3).frame(height: 115).contentShape(Rectangle()).onTapGesture {
                withAnimation {
                    self.isExpanded.toggle()
                }
            }
            Button(action: {
            }) {
                ZStack {
                    RoundedRectangle(cornerRadius: 50)
                        .fill(Color(red: 1.0, green: 0.0, blue: 1.0, opacity: 1.0)).frame(height: 70)
                        .cornerRadius(8)
                        .shadow(radius: 3)
                        Text("Test")
                }
            }.padding([.leading, .trailing], 12)
                .padding(.top, 6)
                .frame(height: 70)
                .buttonStyle(BorderlessButtonStyle())
                .offset(y: self.isExpanded ? 0 : 40)
                .disabled(!self.isExpanded)
        }.frame(height: self.isExpanded ? 120 : 200)
    }
}
