//
//  pdl_det_resto_catatanpembayaran.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 28/11/22.
//

import SwiftUI

struct pdl_det_resto_catatanpembayaran: View {
    @State private var isExpanded = false
    let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .long
            return formatter
        }()

        @State private var birthDate = Date()
    @State private var cap: String = ""
    @State private var cap2: String = ""
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                            HStack(){
                                NavigationLink(destination: pdl_det_resto_det_npwpd().navigationBarHidden(true)) {
                                    Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                        .frame(alignment: .leading)
                                        .padding([.bottom],5)
                                }
                                
                                Text("Catatan Pembayaran")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                            }
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Text("Kost ASDF - 200.30.201.12")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontblack"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.bottom, .top],10)
                                    .padding([.leading, .trailing],20)
                                
                                HStack() {
                                    VStack() {
                                        DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date) {
                                        }
                                        .multilineTextAlignment(.leading)
                                        .frame(width: 127, height: 40)
                                        ////                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        ////                                              .padding(15)
                                        ////                                            .foregroundColor(Color.gray)
                                        //                                        .cornerRadius(15)
                                        //                                        .overlay(
                                        //                                            RoundedRectangle(cornerRadius: 15)
                                        //                                                .stroke(Color("fontgray"), lineWidth: 1)
                                        //                                        )
                                    }
                                    VStack() {
                                        DisclosureGroup("Select", isExpanded: $isExpanded) {
                                            VStack() {
                                                ForEach(1...5, id: \.self) {
                                                    num in
                                                    Text("\(num)")
                                                        .font(.system(size: 15))
                                                        .padding(.all)
                                                }
                                            }
                                        }.accentColor(Color("fontblack"))
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .padding(.all)
                                            .cornerRadius(15)
                                            .frame(width: .infinity, height: 40, alignment: .center)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Image(uiImage: #imageLiteral(resourceName: "iconFilter"))
                                        .frame(alignment: .trailing)
                                        .frame(width: 40, height: 40, alignment: .center)
                                    //                                            .padding([.bottom],5)
                                }.padding([.leading, .trailing],20)
                                ScrollView(.vertical){
                                    NavigationLink(
                                        destination: pdl_det_resto_catatanpembayaran().navigationBarHidden(true)) {
                                            HStack() {
                                                VStack(){
                                                    Text("No SSPD: 1234")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("NO. SSPT : A-11234")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Masa Pajak: Juni 2022")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                                VStack(){
                                                    Text("Rp. 2.000.000")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .center)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.center)
                                                    Text("Total Pajak yang dibayar")
                                                        .font(.system(size: 10))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .center)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.center)
                                                }.frame(maxWidth: .infinity, alignment: .trailing)
                                            }
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                    
                                }
                                Spacer()
                                HStack {
                                    Spacer()
                                    Button(action: {
                                        //Place your action here
                                    }) {
                                        Image("iconDownload")
                                            .font(.system(size: 50))
//                                            .foregroundColor(Color("PrimaryColor"))
                                        
//                                            .shadow(color: .gray, radius: 0.2, x: 1, y: 1)
                                            .padding()
                                    }
                                }.frame(alignment: .trailing)
                            }
                        }
                    }
                }
        }
    }
}

struct pdl_det_resto_catatanpembayaran_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_resto_catatanpembayaran()
    }
}
