//
//  pdl_det_resto_pengajuan.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI

struct pdl_det_resto_pengajuan: View {
    
    @State private var textfield: String = ""
    @State private var nikn: String = ""
    @State private var nikn2: String = ""
    
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State private var newEatenMealTime = Date()
    @State private var showingNewMealTime = false
    @State private var showingNewMealTime2 = false
    @State var isChecked:Bool = false
    
    func toggle(){isChecked = !isChecked}
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
//                    Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                        .resizable()
//                        .frame(width: .infinity, height: 200, alignment: .center)
//                        .edgesIgnoringSafeArea(.all)
                    
                    VStack() {
                        ZStack() {
                            
//                            Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                                .resizable()
//                                .frame(width: .infinity, height: 50)
//                                .edgesIgnoringSafeArea(.all)
                            VStack(){
                                HStack(){
                                    NavigationLink(destination: pdl_det_resto().navigationBarHidden(true)) {
                                        Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                            .frame(alignment: .leading)
                                            .padding([.bottom],5)
                                    }
                                    
                                    Text("Pengajuan")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontwhite"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],5)
                                }
                            }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        }.padding(.bottom, 5)
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Spacer()
                               
                                    Picker("", selection: $segmentationSelection) {
                                        Text("Pengurangan").tag(0)
                                        Text("Keberatan").tag(1)
                                            }
                                    .pickerStyle(SegmentedPickerStyle())
                                    .onAppear{
                                        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(named: "NavColor")
                                    }
                                    .padding([.leading, .trailing])
                                    TabView(selection: $segmentationSelection){
                                        VStack{
                                            ScrollView(.vertical){
                                                Group{
                                                    HStack{
                                                        HStack{
                                                            Text("SPTPD/SKPD")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color("fontblack"))
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.leading])
                                                                .padding([.top],10)
                                                            Text("*")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color.red)
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.top],10)
                                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                                    }.padding([.leading, .trailing],10)
                                                    HStack{
                                                        TextField("Search", text: $textfield)
                                                            .padding(10)
                                                            .foregroundColor(Color.gray)
                                                            .cornerRadius(15)
                                                        
                                                            .padding([.leading, .trailing],10)
                                                        Button(action:{self.showingNewMealTime = true})
                                                        {
                                                            Image(systemName: "magnifyingglass.circle.fill").foregroundColor(.gray)
                                                        }
                                                        .navigationBarHidden(true)
                                                        .padding(.trailing, 10)
                                                        
                                                    }.overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Besat Ketetapan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    TextField("", text: $textfield)
                                                        .padding(10)
                                                        .foregroundColor(Color.gray)
                                                        .cornerRadius(15)
                                                        .overlay(
                                                            RoundedRectangle(cornerRadius: 15)
                                                                .stroke(Color("fontblack"), lineWidth: 1)
                                                        )
                                                        .padding([.leading, .trailing],10)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Alasan Keberatan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    TextField("Kolom", text: $textfield)
                                                        .padding(10)
                                                        .foregroundColor(Color.gray)
                                                        .cornerRadius(15)
                                                        .overlay(
                                                            RoundedRectangle(cornerRadius: 15)
                                                                .stroke(Color("fontblack"), lineWidth: 1)
                                                        )
                                                        .padding([.leading, .trailing],10)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Surat Permohonan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Foto KTP")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Laporan Keuangan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Laporan Pengeluaran")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Dokumen Lainnya")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Spacer()
                                            }
                                        }.tag(0)
                                        .padding([.trailing, .leading], 20)
                                        .padding([.top], 10)
                                        
                                        VStack{
                                            ScrollView(.vertical){
                                                Group{
                                                    HStack{
                                                        HStack{
                                                            Text("SPTPD/SKPD")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color("fontblack"))
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.leading])
                                                                .padding([.top],10)
                                                            Text("*")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color.red)
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.top],10)
                                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                                    }.padding([.leading, .trailing],10)
                                                    HStack{
                                                        TextField("Search", text: $textfield)
                                                            .padding(10)
                                                            .foregroundColor(Color.gray)
                                                            .cornerRadius(15)
                                                        
                                                            .padding([.leading, .trailing],10)
                                                        Button(action:{self.showingNewMealTime = true})
                                                        {
                                                            Image(systemName: "magnifyingglass.circle.fill").foregroundColor(.gray)
                                                        }
                                                        .navigationBarHidden(true)
                                                        .padding(.trailing, 10)
                                                        
                                                    }.overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Besat Ketetapan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    TextField("", text: $textfield)
                                                        .padding(10)
                                                        .foregroundColor(Color.gray)
                                                        .cornerRadius(15)
                                                        .overlay(
                                                            RoundedRectangle(cornerRadius: 15)
                                                                .stroke(Color("fontblack"), lineWidth: 1)
                                                        )
                                                        .padding([.leading, .trailing],10)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Alasan Keberatan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    TextField("Kolom", text: $textfield)
                                                        .padding(10)
                                                        .foregroundColor(Color.gray)
                                                        .cornerRadius(15)
                                                        .overlay(
                                                            RoundedRectangle(cornerRadius: 15)
                                                                .stroke(Color("fontblack"), lineWidth: 1)
                                                        )
                                                        .padding([.leading, .trailing],10)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Surat Permohonan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Foto KTP")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Laporan Keuangan")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Laporan Pengeluaran")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Group{
                                                    HStack{
                                                        Text("Dokumen Lainnya")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color("fontblack"))
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.leading])
                                                            .padding([.top],10)
                                                        Text("*")
                                                            .font(.system(size: 15))
                                                            .foregroundColor(Color.red)
                                                            .multilineTextAlignment(.leading)
                                                            .padding([.top],10)
                                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                                    
                                                    Button(action:{})
                                                    {
                                                        Image(systemName: "doc.circle").foregroundColor(.gray).padding(10)
                                                    }
                                                    .padding([.top, .bottom], 3)
                                                    .frame(maxWidth: .infinity)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                                    .navigationBarHidden(true)
                                                }
                                                Spacer()
                                            }
                                        }.tag(1)
                                        .padding([.trailing, .leading], 20)
                                        .padding([.top], 10)
                                       
                                    }
                                    .tabViewStyle(.page(indexDisplayMode: .never))
                                    .animation(.easeIn, value: segmentationSelection)
                                        
                                Button(action:{self.showingNewMealTime2 = true})
                                {
                                    Text("Submit")
                                        .font(.title3)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("BgColor"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("PrimaryColor"))
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color("PrimaryColor"), lineWidth: 1)
                                        )
                                    }.navigationBarHidden(true)
                                    .padding([.leading, .trailing],10)
                                    .padding([.bottom],20)
                        }
                    }
                }
                    if $showingNewMealTime.wrappedValue {
                        VStack() {
                            ZStack{
                                Color.black.opacity(0.4)
                                    .edgesIgnoringSafeArea(.vertical)
                                // this one is it
                                VStack(spacing: 20) {
                                    Group{
                                        
                                        HStack{
                                                Text("Daftar SPTPD/SKPD Belum Bayar")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.leading)
                                                    .padding([.leading])
                                                    .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading).padding([.leading, .trailing],10)
                                        HStack{
                                            TextField("Search", text: $textfield)
                                                .padding(10)
                                                .foregroundColor(Color.gray)
                                                .cornerRadius(15)
                                            
                                                .padding([.leading, .trailing],10)
                                            Button(action:{self.showingNewMealTime = true})
                                            {
                                                Image(systemName: "magnifyingglass.circle.fill").foregroundColor(.gray)
                                            }
                                            .navigationBarHidden(true)
                                            .padding(.trailing, 10)
                                            
                                        }.overlay(
                                            RoundedRectangle(cornerRadius: 15)
                                                .stroke(Color("fontblack"), lineWidth: 1)
                                        )
                                        .padding([.leading, .trailing],10)
                                        
                                        Divider()
                                            .padding([.leading, .trailing])
                                            .padding([.bottom, .top],5)
                                        ScrollView(.vertical){
                                                Text("123.43534.2.12.33")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .frame(alignment: .leading)
                                                    .multilineTextAlignment(.leading)
                                                Text("No. SKPD: A-11234")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.leading)
                                                Text("Masa Pajak: Juli 2022")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.leading)
                                                Text("Jumlah Pajak: Rp. 10.000.000")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.leading)
                                                Text("Jenis Pajak: Resto")
                                                    .font(.system(size: 12))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.leading)
                                            Divider()
                                                .padding([.leading, .trailing])
                                                .padding([.bottom, .top],5)
                                        }
                                    }
                                    Spacer()
                                    
                                        Button(action: {
                                            self.showingNewMealTime = false
                                        }){
                                            Text("Tutup")
                                                .font(.title3)
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("BgColor"))
                                                .padding()
                                                .frame(maxWidth: .infinity)
                                                .background(Color("PrimaryColor"))
                                                .cornerRadius(10)
                                                .overlay(
                                                    RoundedRectangle(cornerRadius: 10)
                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                )
                                        }.navigationBarHidden(true)
                                            .padding([.leading, .trailing],10)
                                            .padding([.bottom],20)
                                }
                                .frame(width:300, height: 450)
                                .background(Color.white)
                                .mask(RoundedRectangle(cornerRadius: 20))
                                .shadow(radius: 20)
                            }
                        }
                    }
                    if $showingNewMealTime2.wrappedValue{
                        VStack(alignment: .center) {
                            ZStack{
                                Color.black.opacity(0.4)
                                    .edgesIgnoringSafeArea(.vertical)
                                // this one is it
                                VStack(spacing: 20) {
                                    
                                    Image(uiImage: #imageLiteral(resourceName: "IconCheck")).padding(.top, 20)
                                    
                                    Text("Selesai!")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontblack"))
                                        .frame(width: .infinity)
                                        .padding([.top], 10)
                                    
                                    Text("Pengajuan anda akan diverifikasi oleh petugas")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("fontblack"))
                                        .frame(width: .infinity)
                                        .padding([.top], 10)
                                    
                                    Spacer()
                                    Button(action: {
                                        self.showingNewMealTime2 = false
                                    }){
                                        Text("Halaman Utama")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("BgColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color("PrimaryColor"))
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                                            )
                                        }.navigationBarHidden(true)
                                        .padding([.leading, .trailing],10)
                                        .padding([.bottom],20)
                                }
                                .frame(width:300, height: 300)
                                .background(Color.white)
                                .mask(RoundedRectangle(cornerRadius: 20))
                                .shadow(radius: 20)
                            }
                        }
                    }
            }
        }
    }
}

struct pdl_det_resto_pengajuan_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_resto_pengajuan()
    }
}

