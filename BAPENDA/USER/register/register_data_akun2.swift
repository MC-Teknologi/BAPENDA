//
//  register_data_akun2.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 28/11/22.
//

import SwiftUI

struct register_data_akun2: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var cap: String = ""
    @State private var cap2: String = ""
    @State private var cap3: String = ""
    @State private var cap4: String = ""
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
                        NavigationLink(destination: register().navigationBarHidden(true)) {
                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding([.bottom],5)
                                        }
                        
                        Text("Data Akun")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                            
                        
                        Text("Silahkan mengisi data akun dengan benar")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            Group {
                                Text("Username")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.top],20)
                                
                                TextField("Username", text: $cap)
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                            Group{
                                Text("Alamat E-mail")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                
                                TextField("Alamat E-mail", text: $cap2)
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                                Text("Kata Sandi")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                
                                SecureField("Kata Sandi", text: $cap3)
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            Group{
                                Text("Ulangi Kata Sandi")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                
                                SecureField("Ulangi Kata Sandi", text: $cap4)
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                                
                               
                            }
                            Spacer()
                            
                            NavigationLink(
                                destination: register_foto_akun().navigationBarHidden(true),
                                label: {
                                    Text("Selanjutnya")
                                        .font(.title3)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("fontwhite"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("PrimaryColor"))
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color("fontwhite"), lineWidth: 1)
                                        )
                                    
                                })
                            .navigationBarHidden(true)
                            .padding([.leading, .trailing, .bottom],10)
                            .padding([.bottom],10)
                            
                        }
                    }
                }
            }
        }
    }
}

struct register_data_akun2_Previews: PreviewProvider {
    static var previews: some View {
        register_data_akun2()
    }
}
