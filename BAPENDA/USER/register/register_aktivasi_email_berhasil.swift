//
//  register_aktivasi_email_berhasil.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 24/11/22.
//

import SwiftUI

struct register_aktivasi_email_berhasil: View {
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
                        NavigationLink(destination: register_data_akun2().navigationBarHidden(true)) {
                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding([.bottom],5)
                        }
                        
                        Text("Terimakasih")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                        
                        
                        Text("Terimakasih, akun anda berhasil di aktivasi")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                        
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    
                    Spacer()
                    
                    Image(uiImage: #imageLiteral(resourceName: "BerhasilAktivasiImage"))
                        .padding([.bottom],20)
                    
                    Spacer()
                    
                    NavigationLink(
                        destination: sign_in().navigationBarHidden(true),
                        label: {
                            Text("Masuk")
                                .font(.title3)
                                .fontWeight(.bold)
                                .foregroundColor(Color("fontwhite"))
                                .padding()
                                .frame(maxWidth: .infinity)
                                .background(Color("PrimaryColor"))
                                .cornerRadius(10)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 10)
                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                )
                            
                        })
                    .navigationBarHidden(true)
                    .padding([.leading, .trailing],40)
                    .padding([.bottom],10)
                    
                }
            }
        }
    }
}

struct register_aktivasi_email_berhasil_Previews: PreviewProvider {
    static var previews: some View {
        register_aktivasi_email_berhasil()
    }
}
