//
//  register_foto_akun.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 24/11/22.
//

import SwiftUI

struct register_foto_akun: View {
    
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
                        NavigationLink(destination: register_data_akun2().navigationBarHidden(true)) {
                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding([.bottom],5)
                        }
                        
                        Text("Foto KTP")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                        
                        
                        Text("Silahkan masukkan Foto KTP anda")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                        
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            Spacer()
                            
                            Image(uiImage: #imageLiteral(resourceName: "KTPSuccessImage"))
                            
                            Text("Unggah Foto KTP anda")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .frame(maxWidth: .infinity, alignment: .center)
                                .multilineTextAlignment(.leading)
                                .padding([.leading])
                                .padding([.bottom],5)
                            
                            NavigationLink(
                                destination: register_foto_akun().navigationBarHidden(true),
                                label: {
                                    Text("Cari File")
                                        .font(.title3)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("fontwhite"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("PrimaryColor"))
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color("fontwhite"), lineWidth: 1)
                                        )
                                    
                                })
                            .navigationBarHidden(true)
                            .padding([.leading, .trailing, .bottom],30)
                            Spacer()
                            
                            NavigationLink(
                                destination: register_foto_akun_sukses().navigationBarHidden(true),
                                label: {
                                    Text("Daftar")
                                        .font(.title3)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("fontwhite"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color.gray)
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color.gray, lineWidth: 1)
                                        )
                                    
                                })
                            .navigationBarHidden(true)
                            .padding([.leading, .trailing, .bottom],10)
                            
                            
                        }
                    }
                }
            }
        }
    }
}

struct register_foto_akun_Previews: PreviewProvider {
    static var previews: some View {
        register_foto_akun()
    }
}
