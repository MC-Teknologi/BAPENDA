//
//  register.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 19/11/22.
//

import SwiftUI

struct register: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var cap: String = ""
    @State private var cap2: String = ""
    @State private var isExpanded = false
    @State private var isExpanded2 = false
    @State private var isExpanded3 = false
    @State private var isExpanded4 = false
    
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
                        NavigationLink(destination: sign_in().navigationBarHidden(true)) {
                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding([.bottom],5)
                                        }
                        
                        Text("Data Alamat")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                            
                        
                        Text("Silahkan mengisi data alamat dengan benar")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            Group {
                                Text("Provinsi")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.top],20)
                                
                                DisclosureGroup("Provinsi", isExpanded: $isExpanded) {
                                    VStack() {
                                        ForEach(1...5, id: \.self) {
                                            num in
                                            Text("\(num)")
                                                .font(.system(size: 15))
                                                .padding(.all)
                                        }
                                    }
                                }.accentColor(Color("fontblack"))
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .padding(.all)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                            Group{
                                Text("Kabupaten / Kota")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                
                                DisclosureGroup("Kabupaten / Kota", isExpanded: $isExpanded2) {
                                    VStack() {
                                        ForEach(1...5, id: \.self) {
                                            num in
                                            Text("\(num)")
                                                .font(.system(size: 15))
                                                .padding(.all)
                                        }
                                    }
                                }.accentColor(Color("fontblack"))
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .padding(.all)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                            Group{
                                Text("Kecamatan")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                
                                DisclosureGroup("Kecamatan", isExpanded: $isExpanded3) {
                                    VStack() {
                                        ForEach(1...5, id: \.self) {
                                            num in
                                            Text("\(num)")
                                                .font(.system(size: 15))
                                                .padding(.all)
                                        }
                                    }
                                }.accentColor(Color("fontblack"))
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .padding(.all)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                            Group{
                                Text("Kelurahan")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                
                                DisclosureGroup("Kelurahan", isExpanded: $isExpanded4) {
                                    VStack() {
                                        ForEach(1...5, id: \.self) {
                                            num in
                                            Text("\(num)")
                                                .font(.system(size: 15))
                                                .padding(.all)
                                        }
                                    }
                                }.accentColor(Color("fontblack"))
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .padding(.all)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                            Group{
                                Text("Jalan")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                
                                TextField("Jalan/Kav/No", text: $cap)
                                    .padding(15)
                                    .foregroundColor(Color.gray)
                                    .cornerRadius(15)
                                    .overlay(
                                        RoundedRectangle(cornerRadius: 15)
                                            .stroke(Color("fontblack"), lineWidth: 1)
                                    )
                                    .padding([.leading, .trailing],10)
                            }
                            HStack() {
                                VStack() {
                                    Text("RT/RW")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("fontblack"))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                    
                                    TextField("RT/RW", text: $cap)
                                        .padding(15)
                                        .foregroundColor(Color.gray)
                                        .cornerRadius(15)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 15)
                                                .stroke(Color("fontblack"), lineWidth: 1)
                                        )
                                        .padding([.leading, .trailing],10)
                                }
                                VStack() {
                                    Text("Kode Pos")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("fontblack"))
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                    
                                    TextField("Kode Pos", text: $cap2)
                                        .padding(15)
                                        .foregroundColor(Color.gray)
                                        .cornerRadius(15)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 15)
                                                .stroke(Color("fontblack"), lineWidth: 1)
                                        )
                                        .padding([.leading, .trailing],10)
                                }
                            }
                            Spacer()
                            
                            NavigationLink(
                                destination: register_data_akun2().navigationBarHidden(true),
                                label: {
                                    Text("Selanjutnya")
                                        .font(.title3)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("fontwhite"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("PrimaryColor"))
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color("fontwhite"), lineWidth: 1)
                                        )
                                    
                                })
                            .navigationBarHidden(true)
                            .padding([.leading, .trailing, .bottom],10)
                            .padding([.bottom],10)
                        }
                    }
                }
            }
        }
    }
}

struct register_Previews: PreviewProvider {
    static var previews: some View {
        register()
    }
}
