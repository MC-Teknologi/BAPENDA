//
//  register_aktivasi_email.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 24/11/22.
//

import SwiftUI

struct register_aktivasi_email: View {
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
                        NavigationLink(destination: register_data_akun2().navigationBarHidden(true)) {
                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding([.bottom],5)
                        }
                        
                        Text("Selesai")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                        
                        
                        Text("Pendaftaran selesai, silahkan Verifikasi akun")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                        
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            Spacer()
                            
                            Image(uiImage: #imageLiteral(resourceName: "BerhasilEmailImage"))
                                .padding([.bottom],20)
                            
                            Text("Kami sudah mengirimkan tautan aktivasi ke email anda, silahkan melakukan aktivasi untuk membantu anda memulai Aplikasi Pajak Daerah")
                                .font(.system(size: 15))
                                .foregroundColor(Color("fontblack"))
                                .frame(maxWidth: .infinity, alignment: .center)
                                .multilineTextAlignment(.leading)
                                .padding([.leading, .trailing],40)
                                .padding([.bottom],10)
                            
                            NavigationLink(
                                destination: register_aktivasi_email_berhasil().navigationBarHidden(true),
                                label: {
                                    Text("Buka E-mail")
                                        .font(.title3)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("fontwhite"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("PrimaryColor"))
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(Color.gray, lineWidth: 1)
                                        )
                                    
                                })
                            .navigationBarHidden(true)
                            .padding([.leading, .trailing],40)
                            .padding([.bottom],10)
                            
                            HStack() {
                                
                                Text("Tidak mendapatkan e-mail? Silahkan periksa Email Spam anda.")
                                    .font(.system(size: 15))
                                    .foregroundColor(Color("fontblack"))
                                
                                NavigationLink(destination: register_aktivasi_email_berhasil().navigationBarHidden(true)) {
                                    Text("Kirim Ulang")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("PrimaryColor"))
                                }
                            }.padding([.leading, .trailing],40)
                            Spacer()
                            Spacer()
                        }
                    }
                }
            }
        }
    }
}

struct register_aktivasi_email_Previews: PreviewProvider {
    static var previews: some View {
        register_aktivasi_email()
    }
}
