//
//  welcome_page.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 18/11/22.
//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  
//

import SwiftUI

struct welcome_page: View {
    var body: some View {
        NavigationView {
            ZStack {
                Color("BgColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    HStack(){
                        Image(uiImage: #imageLiteral(resourceName: "logo"))
        
                        Text("Badan Pendapatan Daerah Kota Malang")
                            .font(.system(size: 15))
                            .foregroundColor(Color("fontblack"))
                            .frame(maxWidth: 200, alignment: .leading)
                            
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    Text("Selamat Datang!")
                        .font(.system(size: 25))
                        .foregroundColor(Color("fontblack"))
                        .fontWeight(.bold)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .multilineTextAlignment(.leading)
                        .padding([.leading])
                        
                    
                    Text("Di Aplikasi Pajak Daerah Kota Malang")
                        .font(.system(size: 20))
                        .foregroundColor(Color("fontblack"))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .multilineTextAlignment(.leading)
                        .padding([.leading])
                    
                    Image(uiImage: #imageLiteral(resourceName: "bgicon"))
//                        .resizable()
                        .scaledToFill()
//                        .frame(maxWidth: .infinity)
                        .padding()
                    
                    Spacer()
                    NavigationLink(
                        destination: sign_in().navigationBarHidden(true),
                        label: {
                            Text("Masuk")
                                .font(.title3)
                                .fontWeight(.bold)
                                .foregroundColor(Color("PrimaryColor"))
                                .padding()
                                .frame(maxWidth: .infinity)
                                .background(Color.white)
                                .cornerRadius(10)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 10)
                                            .stroke(Color("PrimaryColor"), lineWidth: 1)
                                    )
                                
                        })
                        .navigationBarHidden(true)
                        .padding([.leading, .trailing],10)
                    
                    NavigationLink(
                        destination: register().navigationBarHidden(true),
                        label: {
                            Text("Daftar")
                                .font(.title3)
                                .fontWeight(.bold)
                                .foregroundColor(Color.white)
                                .padding()
                                .frame(maxWidth: .infinity)
                                .background(Color("PrimaryColor"))
                                .cornerRadius(10)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 10)
                                            .stroke(Color.white, lineWidth: 1)
                                    )
                                
                        })
                        .navigationBarHidden(true)
                        .padding([.leading, .trailing, .bottom],10)
                    
                }
                .padding()
            }
        }
    }
}

struct welcome_page_Previews: PreviewProvider {
    static var previews: some View {
        welcome_page()
    }
}
struct HeaderLogoButton: View {
    var image: Image
    var text: Text
    
    var body: some View {
        HStack {
            image
                .padding(.horizontal)
            Spacer()
            text
                .font(.title2)
            Spacer()
        }
        .padding()
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .cornerRadius(50.0)
        .shadow(color: Color.black.opacity(0.08), radius: 60, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 16)
    }
}


