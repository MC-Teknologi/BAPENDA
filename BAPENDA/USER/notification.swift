//
//  notification.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI

struct notification: View {
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
                    VStack() {
                        VStack(){
                                Text("Notifikasi")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color("fontwhite"))
                                    .fontWeight(.bold)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .multilineTextAlignment(.leading)
                                    .padding([.leading])
                                    .padding([.bottom],5)
                        }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack(){
                                ScrollView(.vertical){
                                    NavigationLink(
                                        destination: {}) {
                                                VStack(){
                                                    Text("Notifikasi")
                                                        .font(.system(size: 20))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Isi notifikasi")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                    NavigationLink(
                                        destination: {}) {
                                                VStack(){
                                                    Text("Notifikasi")
                                                        .font(.system(size: 20))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Isi notifikasi")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                    NavigationLink(
                                        destination: {}) {
                                                VStack(){
                                                    Text("Notifikasi")
                                                        .font(.system(size: 20))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Isi notifikasi")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                    NavigationLink(
                                        destination: {}) {
                                                VStack(){
                                                    Text("Notifikasi")
                                                        .font(.system(size: 20))
                                                        .foregroundColor(Color("fontblack"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                    Text("Isi notifikasi")
                                                        .font(.system(size: 12))
                                                        .foregroundColor(Color("fontgray"))
                                                        .frame(maxWidth: .infinity, alignment: .leading)
                                                        .padding([.leading],10)
                                                        .multilineTextAlignment(.leading)
                                                }.frame(maxWidth: .infinity, alignment: .leading)
                                        }
                                        .frame(maxWidth: .infinity, minHeight: 120)
                                        .background(Color("BgColor"))
                                        .cornerRadius(10)
                                        .shadow(
                                            color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                        ).padding([.leading, .trailing],20)
                                }
                            }.padding(.top, 20)
                        }
                    }
                }
        }
    }
}

struct notification_Previews: PreviewProvider {
    static var previews: some View {
        notification()
    }
}
