//
//  pdl_det_peneranganjalan_pendaftaran_2.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI
import StepperView

struct pdl_det_peneranganjalan_pendaftaran_2: View {
    
    @State private var textfield: String = ""
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State var isChecked:Bool = false

    
    let steps = [ Text("Pemilik NPWPD").font(.caption).foregroundColor(Color.black),
                  Text("Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Detail Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Lampiran").font(.caption).foregroundColor(Color.black),
                  Text("Selesai").font(.caption).foregroundColor(Color.black)
                ]

    let indicationTypes = [
                            StepperIndicationType.custom(NumberedCircleView(text: "1", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "2", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "3", color: Color.white)),
                            StepperIndicationType.custom(NumberedCircleView(text: "4", color: Color.white)),
                            StepperIndicationType.custom(NumberedCircleView(text: "5", color: Color.white))
                        ]
    

    
        
    func toggle(){isChecked = !isChecked}
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
//                    Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                        .resizable()
//                        .frame(width: .infinity, height: 200, alignment: .center)
//                        .edgesIgnoringSafeArea(.all)
                    
                    VStack() {
                        ZStack() {
                            
//                            Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                                .resizable()
//                                .frame(width: .infinity, height: 50)
//                                .edgesIgnoringSafeArea(.all)
                            VStack(){
                                HStack(){
                                    NavigationLink(destination: pdl_det_peneranganjalan_pendaftaran().navigationBarHidden(true)) {
                                        Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                            .frame(alignment: .leading)
                                            .padding([.bottom],5)
                                    }
                                    
                                    Text("Pendaftaran NPWPD")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontwhite"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],5)
                                }
                            }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        }.padding(.bottom, 10)
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Spacer()
                                StepperView()
                                        .addSteps(steps)
                                        .indicators(indicationTypes)
                                        .stepIndicatorMode(StepperMode.horizontal)
                                        .spacing(50)
                                        .lineOptions(StepperLineOptions.custom(1, Color("PrimaryColor")))
                                        .stepLifeCycles([StepLifeCycle.completed, .pending, .pending, .pending, .pending])
                                        .padding(.top, 50)
                                
                                    Text("Data Objek Pajak")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("fontblack"))
                                        
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom, .top],10)
                                    Divider()
                                        .padding([.leading, .trailing])
                                        .padding([.bottom, .top],5)
                                    
                                ScrollView(.vertical){
                                    Group{
                                        HStack{
                                            Text("Jenis Usaha")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                            Text("*")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color.red)
                                                .multilineTextAlignment(.leading)
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        TextField("Jenis Usaha", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("Nama Objek Pajak")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                            Text("*")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color.red)
                                                .multilineTextAlignment(.leading)
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        TextField("Nama Objek Pajak", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("NOP")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        TextField("NOP", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("Kecamatan")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                            Text("*")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color.red)
                                                .multilineTextAlignment(.leading)
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        DisclosureGroup("Kecamatan", isExpanded: $kota) {
                                            VStack() {
                                                ForEach(1...5, id: \.self) {
                                                    num in
                                                    Text("\(num)")
                                                        .font(.system(size: 15))
                                                        .padding(.all)
                                                }
                                            }
                                        }.accentColor(Color("fontblack"))
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .padding(.all)
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("Kelurahan")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                            Text("*")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color.red)
                                                .multilineTextAlignment(.leading)
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        DisclosureGroup("Kelurahan", isExpanded: $kelurahan) {
                                            VStack() {
                                                ForEach(1...5, id: \.self) {
                                                    num in
                                                    Text("\(num)")
                                                        .font(.system(size: 15))
                                                        .padding(.all)
                                                }
                                            }
                                        }.accentColor(Color("fontblack"))
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .padding(.all)
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("Jalan")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                            Text("*")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color.red)
                                                .multilineTextAlignment(.leading)
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        TextField("Jalan", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("RT/RW")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        TextField("RT/RW", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("No. Telp")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        TextField("No. Telp", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            Text("Tanggal Mulai Usaha")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        TextField("Tanggal Mulai Usaha", text: $textfield)
                                            .padding(10)
                                            .foregroundColor(Color.gray)
                                            .cornerRadius(15)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 15)
                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                            )
                                            .padding([.leading, .trailing],10)
                                    }
                                    Group{
                                        HStack{
                                            VStack{
                                                Text("Jam Buka")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.leading)
                                                    .padding([.leading])
                                                    .padding([.top],10)
                                                TextField("Jam Buka", text: $textfield)
                                                    .padding(10)
                                                    .foregroundColor(Color.gray)
                                                    .cornerRadius(15)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                            }.multilineTextAlignment(.leading)
                                            VStack{
                                                Text("Jam Tutup")
                                                    .font(.system(size: 15))
                                                    .foregroundColor(Color("fontblack"))
                                                    .multilineTextAlignment(.leading)
                                                    .padding([.leading])
                                                    .padding([.top],10)
                                                TextField("Jam Tutup", text: $textfield)
                                                    .padding(10)
                                                    .foregroundColor(Color.gray)
                                                    .cornerRadius(15)
                                                    .overlay(
                                                        RoundedRectangle(cornerRadius: 15)
                                                            .stroke(Color("fontblack"), lineWidth: 1)
                                                    )
                                                    .padding([.leading, .trailing],10)
                                            }.multilineTextAlignment(.leading)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                     HStack{
                                        Text("Penrangan Genset")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],10)
                                        Text("*")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color.red)
                                            .multilineTextAlignment(.leading)
                                            .padding([.top],10)
                                         Text(" : ")
                                             .font(.system(size: 15))
                                             .foregroundColor(Color("fontblack"))
                                             .multilineTextAlignment(.leading)
                                             .padding([.leading])
                                             .padding([.top],10)
                                         Button(action: toggle){
                                             HStack{
                                                 Image(systemName: isChecked ? "checkmark.square": "square")
                                                 Text("Ya")
                                                     .font(.system(size: 15))
                                                     .foregroundColor(Color("fontblack"))
                                             }.padding(.top, 10)
                                         }
                                         Button(action: toggle){
                                             HStack{
                                                 Image(systemName: isChecked ? "checkmark.square": "square")
                                                 Text("Tidak")
                                                     .font(.system(size: 15))
                                                     .foregroundColor(Color("fontblack"))
                                             }.padding(.top, 10)
                                         }
                                    }.frame(maxWidth: .infinity, alignment: .leading)
                                        HStack{
                                           Text("Air Tanah")
                                               .font(.system(size: 15))
                                               .foregroundColor(Color("fontblack"))
                                               .multilineTextAlignment(.leading)
                                               .padding([.leading])
                                               .padding([.top],10)
                                           Text("*")
                                               .font(.system(size: 15))
                                               .foregroundColor(Color.red)
                                               .multilineTextAlignment(.leading)
                                               .padding([.top],10)
                                            Text(" : ")
                                                .font(.system(size: 15))
                                                .foregroundColor(Color("fontblack"))
                                                .multilineTextAlignment(.leading)
                                                .padding([.leading])
                                                .padding([.top],10)
                                            Button(action: toggle){
                                                HStack{
                                                    Image(systemName: isChecked ? "checkmark.square": "square")
                                                    Text("Ya")
                                                        .font(.system(size: 15))
                                                        .foregroundColor(Color("fontblack"))
                                                }.padding(.top, 10)
                                            }
                                            Button(action: toggle){
                                                HStack{
                                                    Image(systemName: isChecked ? "checkmark.square": "square")
                                                    Text("Tidak")
                                                        .font(.system(size: 15))
                                                        .foregroundColor(Color("fontblack"))
                                                }.padding(.top, 10)
                                            }
                                       }.frame(maxWidth: .infinity, alignment: .leading)
                                        HStack{
                                        Text("Pilih titik pada peta")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color("fontblack"))
                                            .multilineTextAlignment(.leading)
                                            .padding([.leading])
                                            .padding([.top],10)
                                        Text("*")
                                            .font(.system(size: 15))
                                            .foregroundColor(Color.red)
                                            .multilineTextAlignment(.leading)
                                            .padding([.top],10)
                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                        Image(uiImage: #imageLiteral(resourceName: "map"))
                                            .frame(alignment: .center)
                                            .padding([.bottom],5)                                    }
                                }
                                Spacer()
                                NavigationLink(
                                    destination: pdl_det_peneranganjalan_pendaftaran_3().navigationBarHidden(true),
                                    label: {
                                        Text("Selanjutnya")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("BgColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color("PrimaryColor"))
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                                            )
                                        
                                    })
                                .navigationBarHidden(true)
                                .padding([.leading, .trailing, .bottom],10)
                                .padding([.bottom],10)
                        }
                    }
                }
            }
        }
    }
}

struct pdl_det_peneranganjalan_pendaftaran_2_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_peneranganjalan_pendaftaran_2()
    }
}
