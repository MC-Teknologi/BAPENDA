//
//  pdl_det_peneranganjalan_pendaftaran_3.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 01/12/22.
//

import SwiftUI
import StepperView

struct pdl_det_peneranganjalan_pendaftaran_3: View {
    @State private var textfield: String = ""
    @State private var isDisclosed = false
    @State var segmentationSelection  = 0
    @State private var kota = false
    @State private var kelurahan = false
    @State private var kota2 = false
    @State private var kelurahan2 = false
    @State private var narahubung = false
    @State private var narahubung2 = false
    @State private var kotanarahubung = false
    @State private var kotanarahubung2 = false
    @State private var kelurahannarahubung = false
    @State private var kelurahannarahubung2 = false
    @State private var showingCredits = false
    @State private var showingCredits2 = false
    @State var isChecked:Bool = false

    
    let steps = [ Text("Pemilik NPWPD").font(.caption).foregroundColor(Color.black),
                  Text("Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Detail Objek Pajak").font(.caption).foregroundColor(Color.black),
                  Text("Lampiran").font(.caption).foregroundColor(Color.black),
                  Text("Selesai").font(.caption).foregroundColor(Color.black)
                ]

    let indicationTypes = [
                            StepperIndicationType.custom(NumberedCircleView(text: "1", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "2", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "3", color: Color("PrimaryColor"))),
                            StepperIndicationType.custom(NumberedCircleView(text: "4", color: Color.white)),
                            StepperIndicationType.custom(NumberedCircleView(text: "5", color: Color.white))
                        ]
    

    
        
    func toggle(){isChecked = !isChecked}
    var body: some View {
        NavigationView {
                ZStack {
                    Color("NavColor").edgesIgnoringSafeArea(.all)
//                    Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                        .resizable()
//                        .frame(width: .infinity, height: 200, alignment: .center)
//                        .edgesIgnoringSafeArea(.all)
                    
                    VStack() {
                        ZStack() {
                            
//                            Image(uiImage: #imageLiteral(resourceName: "headnav"))
//                                .resizable()
//                                .frame(width: .infinity, height: 50)
//                                .edgesIgnoringSafeArea(.all)
                            VStack(){
                                HStack(){
                                    NavigationLink(destination: pdl_det_peneranganjalan_pendaftaran_2().navigationBarHidden(true)) {
                                        Image(uiImage: #imageLiteral(resourceName: "back-img"))
                                            .frame(alignment: .leading)
                                            .padding([.bottom],5)
                                    }
                                    
                                    Text("Pendaftaran NPWPD")
                                        .font(.system(size: 20))
                                        .foregroundColor(Color("fontwhite"))
                                        .fontWeight(.bold)
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom],5)
                                }
                            }.frame(maxWidth: .infinity, alignment: .leading).padding()
                        }.padding(.bottom, 10)
                        Spacer()
                        
                        ZStack() {
                            
                            Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                                .resizable()
                                .edgesIgnoringSafeArea(.all)
                            
                            VStack() {
                                Spacer()
                                StepperView()
                                        .addSteps(steps)
                                        .indicators(indicationTypes)
                                        .stepIndicatorMode(StepperMode.horizontal)
                                        .spacing(50)
                                        .lineOptions(StepperLineOptions.custom(1, Color("PrimaryColor")))
                                        .stepLifeCycles([StepLifeCycle.completed, .pending, .pending, .pending, .pending])
                                        .padding(.top, 50)
                                
                                    Text("Data Detail Objek Pajak")
                                        .font(.system(size: 15))
                                        .foregroundColor(Color("fontblack"))
                                        
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .multilineTextAlignment(.leading)
                                        .padding([.leading])
                                        .padding([.bottom, .top],10)
                                    Divider()
                                        .padding([.leading, .trailing])
                                        .padding([.bottom, .top],5)
                                    
                                ScrollView(.vertical){
                                    Button("Tambah Klasifikasi") {
                                            
                                        showingCredits2.toggle()
                                    }
                                        .font(.title3)
                                        .foregroundColor(Color("fontwhite"))
                                        .padding()
                                        .frame(maxWidth: .infinity)
                                        .background(Color("PrimaryColor"))
                                        .cornerRadius(10)
                                        .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("BgColor"), lineWidth: 1)
                                            )
                                        .navigationBarHidden(true)
                                        .padding([.leading, .trailing, .bottom],10)
                                        .sheet(isPresented: $showingCredits2) {
                                            ZStack {
                                                Color("BgColor").edgesIgnoringSafeArea(.all)
                                                VStack() {
                                                    Spacer()
                                                    Group{
                                                        HStack{
                                                            Text("Klasifikasi")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color("fontblack"))
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.leading])
                                                                .padding([.top],10)
                                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                                        
                                                        TextField("Klasifikasi", text: $textfield)
                                                            .padding(10)
                                                            .foregroundColor(Color.gray)
                                                            .cornerRadius(15)
                                                            .overlay(
                                                                RoundedRectangle(cornerRadius: 15)
                                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                                            )
                                                            .padding([.leading, .trailing],10)
                                                    }
                                                    Group{
                                                        HStack{
                                                            Text("Jumlah Kamar")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color("fontblack"))
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.leading])
                                                                .padding([.top],10)
                                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                                        
                                                        TextField("Jumlah Kamar", text: $textfield)
                                                            .padding(10)
                                                            .foregroundColor(Color.gray)
                                                            .cornerRadius(15)
                                                            .overlay(
                                                                RoundedRectangle(cornerRadius: 15)
                                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                                            )
                                                            .padding([.leading, .trailing],10)
                                                    }
                                                    Group{
                                                        HStack{
                                                            Text("Tarif Kamar")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color("fontblack"))
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.leading])
                                                                .padding([.top],10)
                                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                                        
                                                        TextField("Tarif Kamar", text: $textfield)
                                                            .padding(10)
                                                            .foregroundColor(Color.gray)
                                                            .cornerRadius(15)
                                                            .overlay(
                                                                RoundedRectangle(cornerRadius: 15)
                                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                                            )
                                                            .padding([.leading, .trailing],10)
                                                    }
                                                    Group{
                                                        HStack{
                                                            Text("Kapasitas Kamar")
                                                                .font(.system(size: 15))
                                                                .foregroundColor(Color("fontblack"))
                                                                .multilineTextAlignment(.leading)
                                                                .padding([.leading])
                                                                .padding([.top],10)
                                                        }.frame(maxWidth: .infinity, alignment: .leading)
                                                        
                                                        TextField("kapasitas Kamar", text: $textfield)
                                                            .padding(10)
                                                            .foregroundColor(Color.gray)
                                                            .cornerRadius(15)
                                                            .overlay(
                                                                RoundedRectangle(cornerRadius: 15)
                                                                    .stroke(Color("fontblack"), lineWidth: 1)
                                                            )
                                                            .padding([.leading, .trailing],10)
                                                    }
                                                    Spacer()
                                                    NavigationLink(
                                                        destination: pdl_det_peneranganjalan_pendaftaran_31().navigationBarHidden(true),
                                                        label: {
                                                            Text("Tambahkan")
                                                                .font(.title3)
                                                                .fontWeight(.bold)
                                                                .foregroundColor(Color("BgColor"))
                                                                .padding()
                                                                .frame(maxWidth: .infinity)
                                                                .background(Color("PrimaryColor"))
                                                                .cornerRadius(10)
                                                                .overlay(
                                                                    RoundedRectangle(cornerRadius: 10)
                                                                        .stroke(Color("PrimaryColor"), lineWidth: 1)
                                                                )
                                                            
                                                        })
                                                    .navigationBarHidden(true)
                                                    .padding([.leading, .trailing, .bottom],10)
                                                    .padding([.bottom],10)
                                                }
                                            }
                                        }
                                }
                                Spacer()
                                NavigationLink(
                                    destination: pdl_det_peneranganjalan_pendaftaran_31().navigationBarHidden(true),
                                    label: {
                                        Text("Selanjutnya")
                                            .font(.title3)
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("BgColor"))
                                            .padding()
                                            .frame(maxWidth: .infinity)
                                            .background(Color("PrimaryColor"))
                                            .cornerRadius(10)
                                            .overlay(
                                                RoundedRectangle(cornerRadius: 10)
                                                    .stroke(Color("PrimaryColor"), lineWidth: 1)
                                            )
                                        
                                    })
                                .navigationBarHidden(true)
                                .padding([.leading, .trailing, .bottom],10)
                                .padding([.bottom],10)
                        }
                    }
                }
            }
        }
    }
}

struct pdl_det_peneranganjalan_pendaftaran_3_Previews: PreviewProvider {
    static var previews: some View {
        pdl_det_peneranganjalan_pendaftaran_3()
    }
}
