//
//  home.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 24/11/22.
//

import SwiftUI

struct home: View {
    var body: some View {
        NavigationView {
            
            ZStack {
                Color("NavColor").edgesIgnoringSafeArea(.all)
                VStack() {
                    VStack(){
                        
//                        NavigationLink(destination: welcome_page().navigationBarHidden(true)) {
//                            Image(uiImage: #imageLiteral(resourceName: "back-img"))
//                                .frame(maxWidth: .infinity, alignment: .leading)
//                                .padding([.bottom],5)
//                        }
                        
                        Text("Aplikasi Pajak")
                            .font(.system(size: 25))
                            .foregroundColor(Color("fontwhite"))
                            .fontWeight(.bold)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                            .padding([.bottom],5)
                        
                        
                        Text("Aplikasi Pajak Daerah Kota Malang")
                            .font(.system(size: 18))
                            .foregroundColor(Color("fontwhite"))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                            .padding([.leading])
                        
                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
                    Spacer()
                    
                    ZStack() {
                        
                        Image(uiImage: #imageLiteral(resourceName: "BgFormImage"))
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                        
                        VStack() {
                            HStack() {
                                NavigationLink(
                                    destination: pajak_daerah_lainnya().navigationBarHidden(true)) {
                                        VStack() {
                                            Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                            Text("Pajak Daerah Lainnya")
                                                .font(.system(size: 17))
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("fontblack"))
                                        }
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 130)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    )
                                    
                                
                                NavigationLink(
                                    destination: {}) {
                                        VStack() {
                                            Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                            Text("Pajak PBB")
                                                .font(.system(size: 17))
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("fontblack"))
                                        }
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 130)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    )
                                    
                                
                                NavigationLink(
                                    destination: {}) {
                                        VStack() {
                                            Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                            Text("Pajak BPHTB")
                                                .font(.system(size: 17))
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("fontblack"))
                                        }
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 130)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    )
                            }
                                .padding([.leading, .trailing],20)
                                .padding([.top],20)
                            HStack() {
                                NavigationLink(
                                    destination: {}) {
                                        VStack() {
                                            Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                            Text("Lapor E-SPTPD")
                                                .font(.system(size: 17))
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("fontblack"))
                                        }
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 130)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    )
                                    
                                NavigationLink(
                                    destination: {}) {
                                        VStack() {
                                            Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                            Text("Pendaftaran NPWPD")
                                                .font(.system(size: 17))
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("fontblack"))
                                        }
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 130)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    )
                                    
                                NavigationLink(
                                    destination: {}) {
                                        VStack() {
                                            Image(uiImage: #imageLiteral(resourceName: "iconkosong"))
                                            Text("Pendaftaran NOP")
                                                .font(.system(size: 17))
                                                .fontWeight(.bold)
                                                .foregroundColor(Color("fontblack"))
                                        }
                                    }
                                    .frame(maxWidth: .infinity, minHeight: 130)
                                    .background(Color("BgColor"))
                                    .cornerRadius(10)
                                    .shadow(
                                        color: Color.black.opacity(0.6), radius: 3, x: 0.0, y: 3
                                    )
                                    
                            }.padding([.leading, .trailing],20)
                            Spacer()
                            
                            
                            
                        }
                    }
                }
            }
        }
    }
}

struct home_Previews: PreviewProvider {
    static var previews: some View {
        home()
    }
}
