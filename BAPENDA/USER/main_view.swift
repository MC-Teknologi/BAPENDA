//
//  main_view.swift
//  BAPENDA
//
//  Created by PT. Mutiara Cemerlang Teknologi on 24/11/22.
//

import SwiftUI

struct main_view: View {
//    init() {
//        UITabBar.appearance().backgroundColor = UIColor.gray
//      }
    @State var selectedTab = 0
    var body: some View {
        TabView(selection: $selectedTab) {
            
            home()
                .tag(0)
                .tabItem {
                    Image(systemName: "house.fill")
                        Text("Beranda")
                }
         
            chat()
                .tag(1)
                .tabItem {
                    Image(systemName: "ellipsis.bubble")
                    Text("Chat")
                }
            
            notification()
                .tag(2)
                .tabItem {
                    Image(systemName: "bell.fill")
                    Text("Notifikasi")
                }
            
            profile()
                .tag(3)
                .tabItem {
                    Image(systemName: "person.fill")
                    Text("Profil")
                }
        }.onAppear() {
//            UITabBar.appearance().backgroundColor = UIColor.gray
            let standardAppearance = UITabBarAppearance()
            standardAppearance.backgroundColor = UIColor(Color.white)
            standardAppearance.shadowColor = UIColor(Color.gray)
            
            let itemAppearance = UITabBarItemAppearance()
            itemAppearance.normal.iconColor = UIColor(Color.black)
            itemAppearance.selected.iconColor = UIColor(Color.blue)
            itemAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(Color.black)]
            itemAppearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(Color.blue)]
            standardAppearance.inlineLayoutAppearance = itemAppearance
            standardAppearance.stackedLayoutAppearance = itemAppearance
            standardAppearance.compactInlineLayoutAppearance = itemAppearance
            UITabBar.appearance().standardAppearance = standardAppearance
        }
    }
}

struct main_view_Previews: PreviewProvider {
    static var previews: some View {
        main_view()
    }
}
