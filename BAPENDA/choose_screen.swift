//
//  choose_screen.swift
//  BAPENDA
//
//  Created by Pijar Dwi Kusuma on 05/12/22.
//

import SwiftUI

struct choose_screen: View {
    var body: some View {
        NavigationView {
            ZStack {
                Color("BgColor").edgesIgnoringSafeArea(.all)
                VStack() {
//                    HStack(){
//                        Image(uiImage: #imageLiteral(resourceName: "logo"))
//
//                        Text("Badan Pendapatan Daerah Kota Malang")
//                            .font(.system(size: 15))
//                            .foregroundColor(Color("fontblack"))
//                            .frame(maxWidth: 200, alignment: .leading)
//
//                    }.frame(maxWidth: .infinity, alignment: .leading).padding()
//                    Spacer()
//                    Text("Selamat Datang!")
//                        .font(.system(size: 25))
//                        .foregroundColor(Color("fontblack"))
//                        .fontWeight(.bold)
//                        .frame(maxWidth: .infinity, alignment: .leading)
//                        .multilineTextAlignment(.leading)
//                        .padding([.leading])
//
//
//                    Text("Di Aplikasi Pajak Daerah Kota Malang")
//                        .font(.system(size: 20))
//                        .foregroundColor(Color("fontblack"))
//                        .frame(maxWidth: .infinity, alignment: .leading)
//                        .multilineTextAlignment(.leading)
//                        .padding([.leading])
//
//                    Image(uiImage: #imageLiteral(resourceName: "bgicon"))
//                        .scaledToFill()
//                        .padding()
//
                    Spacer()
                    NavigationLink(
                        destination: welcome_page().navigationBarHidden(true),
                        label: {
                            Text("User")
                                .font(.title3)
                                .fontWeight(.bold)
                                .foregroundColor(Color("PrimaryColor"))
                                .padding()
                                .frame(maxWidth: .infinity)
                                .background(Color.white)
                                .cornerRadius(10)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 10)
                                            .stroke(Color("PrimaryColor"), lineWidth: 1)
                                    )
                                
                        })
                        .navigationBarHidden(true)
                        .padding([.leading, .trailing],10)
                    
                    NavigationLink(
                        destination: welcome_pg().navigationBarHidden(true),
                        label: {
                            Text("Pegawai")
                                .font(.title3)
                                .fontWeight(.bold)
                                .foregroundColor(Color.white)
                                .padding()
                                .frame(maxWidth: .infinity)
                                .background(Color("PrimaryColor"))
                                .cornerRadius(10)
                                .overlay(
                                        RoundedRectangle(cornerRadius: 10)
                                            .stroke(Color.white, lineWidth: 1)
                                    )
                                
                        })
                        .navigationBarHidden(true)
                        .padding([.leading, .trailing, .bottom],10)
                    Spacer()
                }
                .padding()
            }
        }
    }
}

struct choose_screen_Previews: PreviewProvider {
    static var previews: some View {
        choose_screen()
    }
}
